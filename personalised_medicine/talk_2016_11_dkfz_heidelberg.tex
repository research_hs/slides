\documentclass[10pt]{beamer}\usepackage[]{graphicx}\usepackage[]{color}
%% maxwidth is the original width if it is less than linewidth
%% otherwise use linewidth (to make sure the graphics do not exceed the margin)
\makeatletter
\def\maxwidth{ %
  \ifdim\Gin@nat@width>\linewidth
    \linewidth
  \else
    \Gin@nat@width
  \fi
}
\makeatother

\definecolor{fgcolor}{rgb}{0.345, 0.345, 0.345}
\newcommand{\hlnum}[1]{\textcolor[rgb]{0.686,0.059,0.569}{#1}}%
\newcommand{\hlstr}[1]{\textcolor[rgb]{0.192,0.494,0.8}{#1}}%
\newcommand{\hlcom}[1]{\textcolor[rgb]{0.678,0.584,0.686}{\textit{#1}}}%
\newcommand{\hlopt}[1]{\textcolor[rgb]{0,0,0}{#1}}%
\newcommand{\hlstd}[1]{\textcolor[rgb]{0.345,0.345,0.345}{#1}}%
\newcommand{\hlkwa}[1]{\textcolor[rgb]{0.161,0.373,0.58}{\textbf{#1}}}%
\newcommand{\hlkwb}[1]{\textcolor[rgb]{0.69,0.353,0.396}{#1}}%
\newcommand{\hlkwc}[1]{\textcolor[rgb]{0.333,0.667,0.333}{#1}}%
\newcommand{\hlkwd}[1]{\textcolor[rgb]{0.737,0.353,0.396}{\textbf{#1}}}%
\let\hlipl\hlkwb

\usepackage{framed}
\makeatletter
\newenvironment{kframe}{%
 \def\at@end@of@kframe{}%
 \ifinner\ifhmode%
  \def\at@end@of@kframe{\end{minipage}}%
  \begin{minipage}{\columnwidth}%
 \fi\fi%
 \def\FrameCommand##1{\hskip\@totalleftmargin \hskip-\fboxsep
 \colorbox{shadecolor}{##1}\hskip-\fboxsep
     % There is no \\@totalrightmargin, so:
     \hskip-\linewidth \hskip-\@totalleftmargin \hskip\columnwidth}%
 \MakeFramed {\advance\hsize-\width
   \@totalleftmargin\z@ \linewidth\hsize
   \@setminipage}}%
 {\par\unskip\endMakeFramed%
 \at@end@of@kframe}
\makeatother

\definecolor{shadecolor}{rgb}{.97, .97, .97}
\definecolor{messagecolor}{rgb}{0, 0, 0}
\definecolor{warningcolor}{rgb}{1, 0, 1}
\definecolor{errorcolor}{rgb}{1, 0, 0}
\newenvironment{knitrout}{}{} % an empty environment to be redefined in TeX

\usepackage{alltt}

% knit from command line with R CMD Sweave
%\VignetteEngine{knitr::knitr}

\input{./input/head}
\input{./input/commands}

%% above frame title 
\addheadboxtemplate{\color[rgb]{1,1,1}}{\color{uzh} \underline{{\hspace{3pt}\includegraphics[scale=0.33]{uzh_logo_e_pos} 
\hspace{0.7\paperwidth}
\color{black} \tiny Heidi Seibold} \hspace{0.1\paperwidth}}}

%% figures path
\graphicspath{{figure/}}

%% Title
\author{Heidi Seibold}
\title{Stratified and personalised medicine using model-based recursive
partitioning}
\date{}
\institute[]{Epidemiology, Biostatistics and Prevention Institute\\[1em] 
\includegraphics[width=0.3\textwidth]{uzh_logo_e_pos}\\[1em]
Joint work with Torsten Hothorn (UZH) and Achim Zeileis (UIBK), sponsored by SNF
}
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\begin{document}




\setbeamercolor{bgr}{fg=black,bg=uzh}

\thispagestyle{empty}
\begin{frame}
\transsplithorizontalout
%\vspace*{1,5cm}
\titlepage
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%

\begin{frame}{What are \textit{Stratified and Personalised Medicine}?}
\textbf{Stratified medicine}:\\
Find subgroups of patients that differ with respect to their treatment effect.
Estimate the treatment effect for each subgroup.\\[2em]

\textbf{Personalised medicine}:\\
Estimate a personalised treatment effect for each patient.
\end{frame}


%-------------------------------------------------------------------------%

\begin{frame}{Pooled Resource Open-Access ALS Clinical Trials Database}
\begin{columns}
\column{0.6\textwidth}
PRO-ACT database\footnotemark\\[1.5 em] 
\begin{itemize}
\item 23 phase 2 clinical trials
\item Riluzole versus no treatment
\item Primary endpoints of interest:\\
\begin{itemize}
\item	ALS Functional Rating Scale (ALSFRS) 
\item	Survival time
\end{itemize}
\end{itemize}

\column{0.4\textwidth}
\visible<2>{
\hspace*{-2em}\resizebox{1.2\textwidth}{!}{
\begin{tikzpicture}[mindmap, concept color=uzh!50, font=\sf]

  \tikzstyle{level 1 concept}+=[font=\sf \small, level distance = 8.5em]
  \tikzset{every node/.append style={scale=0.65}}
 
  \node[concept, font=\Large] {ALSFRS\\ 0 - 40}
    child[concept color=gray!20, grow = 72]{ node[concept]{speech}}
    child[concept color=gray!20, grow = 36]{ node[concept]{salivation}}
    child[concept color=gray!20, grow = 0]{ node[concept]{swallowing}}
    child[concept color=gray!20, grow = 324]{ node[concept]{handwriting}}
    child[concept color=gray!20, grow = 288]{ node[concept]{cutting food and handling utensils}}
    child[concept color=gray!20, grow = 252]{ node[concept]{dressing and hygiene}}
    child[concept color=gray!20, grow = 216]{ node[concept]{turning in bed and adjusting bed clothes}}
    child[concept color=gray!20, grow = 180]{ node[concept]{walking}}
    child[concept color=gray!20, grow = 144]{ node[concept]{climbing stairs}}
    child[concept color=gray!20, grow = 108]{ node[concept]{breathing}};

\end{tikzpicture}
}
}
\end{columns}
\footnotetext{\scriptsize\url{https://nctu.partners.org/ProACT}}
\end{frame}









\begin{frame}[fragile]{ALSFRS: Normal GLM with log link}
\small
\begin{align*}
  \mE\left.\left(\frac{\alsfrs_6}{\alsfrs_0} \right\vert  X = x\right) =
\frac{\mE(\alsfrs_6\vert  X = x)}{\alsfrs_0} = \exp\{ \alpha + \beta x_{R}\}\\[0.5em]
\end{align*}

\includegraphics[width=\textwidth]{figure/ALSFRS-1} 

\end{frame}


\begin{frame}[fragile]{Survival time: Weibull model}
\small
\begin{align*}
\mP(Y \le y | X = x) = F\left(\frac{\log(y) - \alpha_1 - \beta x_R}
{\alpha_2}\right)\\[0.5em]
\end{align*}

\includegraphics[width=\textwidth]{figure/ALSsurv-1} 

\end{frame}


\begin{frame}
\begin{center}
  \large
  Are there patients for whom Riluzole\\
  has a stronger effect?
\end{center}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%

\begin{frame}{Predictive and prognostic factors}

{
\scriptsize
\begin{tikzpicture}
\node[circle, fill=gray!20, text width=1.6cm, align=center] (prog) at (12-6,9) {prognostic factor $z$};
\node[circle, fill=uzh!40, text width=1.2cm, align=center] (outc) at (15-6,9) {primary endpoint};
\draw[->, line width=2pt] (prog) -- node [midway, above] {$\alpha = \alpha(z)~$} (outc);

\node[circle, fill=gray!20, text width=1.6cm, align=center] (pred) at (6+6,9) {predictive factor $z$};
\node[circle, fill=gray!20, text width=1.2cm, align=center] (treat) at (8.7+6,10.2) {treatment};
\node[circle, fill=gray!20, text width=1.2cm, align=center] (outc2) at (8.7+6,7.8) {primary endpoint};
\draw[->, line width=2pt] (pred) -- node [midway, above] {$\beta = \beta(z)$} (8.5+6,9);
\draw[->, line width=4pt, uzh!50] (treat) -- (outc2);
\end{tikzpicture}
}

\end{frame}


\begin{frame}{Stratified medicine: Subgroup analyses}
\begin{center}
  \resizebox{0.35\textwidth}{!}{
  \begin{tikzpicture}
	\node[ellipse, fill=gray!40, align=center] (n0) at (12, 9) {};
	\node[rectangle, fill=uzh!40, align=center] (n1) at (11, 7) {};
	\draw[-, line width=1pt] (n0) -- (n1);
	\node[ellipse, fill=gray!40, align=center] (n2) at (12.5, 8) {};
	\draw[-, line width=1pt] (n0) -- (n2);
	\node[rectangle, fill=uzh!40, align=center] (n3) at (12, 7) {};
	\draw[-, line width=1pt] (n2) -- (n3);
	\node[rectangle, fill=uzh!40, align=center] (n4) at (13, 7) {};
	\draw[-, line width=1pt] (n2) -- (n4);
	
\end{tikzpicture}
}\\[2em]
\end{center}
\end{frame}



\begin{frame}{MOB Basics}
MOB: Model-based recursive partitioning\\[2em]
Start with model $\model(\theodat, \bftheta)$ with $$\bftheta = \begin{pmatrix}
\bfalpha\\
\bfbeta\\
\end{pmatrix}
\begin{array}{l} 
\text{intercept(s)}\\
\text{treatment effect}\\
\end{array}
$$
which fits data $\theodat$.\\[2em]

{\small
\begin{tabular}{|cc|}
\hline
&\\
ALSFRS: Normal GLM with log link & Survival time: Weibull model\\
$ \frac{\mE(\alsfrs_6\vert  X = x)}{\alsfrs_0} = \exp\{ \alpha + \beta x_{R}\} $ 
&
$ \mP(Y \le y | X = x) = F\left(\frac{\log(y) - \alpha_1 - \beta x_R}
{\alpha_2}\right) $ \\
&\\
\hline
\end{tabular}
}
\end{frame}




\begin{frame}{MOB Basics}
\begin{itemize}
\item[$\Rightarrow$] Find partitions of the data based on patient characteristics $\theoz = (\theoz_1,...,\theoz_J) \in \spacz$ which differ with respect to the model parameters $\bftheta$.
\item[$\Rightarrow$] Fit separate models $\model(\theodat, \bftheta(b))$ in partitions.\\
\end{itemize}\vspace{1em}
{
\center
\begin{tikzpicture}
\node[ellipse, fill=gray!40, align=center] (n0) at (12,9) {$\bftheta$};

\node[rectangle, fill=uzh!40, align=center] (n1) at (11,8) {$\bftheta(1)$};
\draw[-, line width=1pt] (n0) -- (n1);

\node[rectangle, fill=uzh!40, align=center] (n2) at (13,8) {$\bftheta(2)$};
\draw[-, line width=1pt] (n0) -- (n2);
\end{tikzpicture}\\[3em]
}
\end{frame}



\begin{frame}{How to find the Partitions?}
Test of independence between the \textbf{partial score functions} and each \textbf{patient characteristic}:
\begin{align*}
H_0^{\alpha,j}&: \quad \psi_\alpha(\theodat, \htheta) \quad\bot\quad \theoz_j\\
H_0^{\beta,j}&: \quad \psi_\beta(\theodat, \htheta) \quad\bot\quad \theoz_j, \quad j = 1, \dots, J
\end{align*}\\[1em]
$\psi_\alpha, \psi_\beta$ partial derivatives of the log-likelihood with respect to $\bfalpha / \bfbeta$.\\[1.5em]
\begin{itemize}
\item Partition if global p-value smaller than significance level
\item Use as split variable the one with the smallest p-value
\end{itemize}
\end{frame}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%


\begin{frame}{ALSFRS}
\hspace*{-3em}
\includegraphics[width = 1.15\textwidth]{figure4.pdf}
\end{frame}

\begin{frame}{Survival time}
\hspace*{-3em}
\includegraphics[width = 1.15\textwidth]{figure5.pdf}
\end{frame}


\begin{frame}{Summary: MOB for stratified medicine}
\begin{itemize}
  \item Data-driven approach for subgroup analyses.
  \item Easy to understand and communicate.\\[2em]
\end{itemize}

Extensions:
\begin{itemize}
  \item Dose-response models
  \item Mixed models\footnote{M. Fokkema, N. Smits, A. Zeileis, T. Hothorn, H. Kelderman. \textit{Detecting treatment-subgroup interactions in clustered data with generalized linear mixed-effects model trees}. Working Papers in Economics and Statistics of the University of Innsbruck, 2015.}
  \item PALM trees
\end{itemize}


\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%

\begin{frame}{Personalised medicine: Individual treatment
effect prediction}
\small
  Maybe subgroups are still not good enough\\
  $\Rightarrow$ Personalised treatment effects through model-based random forests\\[2em]

\begin{center}
  \resizebox{0.55\textwidth}{!}{
  \begin{tikzpicture}
	\node[ellipse, fill=gray!40, align=center] (n0) at (12, 9) {};
	\node[rectangle, fill=uzh!40, align=center] (n1) at (11.5, 8) {};
	\draw[-, line width=1pt] (n0) -- (n1);
	\node[ellipse, fill=gray!40, align=center] (n2) at (12.5, 8) {};
	\draw[-, line width=1pt] (n0) -- (n2);
	\node[rectangle, fill=uzh!40, align=center] (n3) at (12, 7) {};
	\draw[-, line width=1pt] (n2) -- (n3);
	\node[rectangle, fill=uzh!40, align=center] (n4) at (13, 7) {};
	\draw[-, line width=1pt] (n2) -- (n4);
	
	\node[ellipse, fill=gray!40, align=center] (n1_0) at (12+3.5, 9) {};
	\node[ellipse, fill=gray!40, align=center] (n1_1) at (11.5+3.5, 8) {};
	\draw[-, line width=1pt] (n1_0) -- (n1_1);
	\node[rectangle, fill=uzh!40, align=center] (n1_2) at (12.5+3.5, 8) {};
	\draw[-, line width=1pt] (n1_0) -- (n1_2);
	\node[ellipse, fill=gray!40, align=center] (n1_3) at (11+3.5, 7) {};
	\draw[-, line width=1pt] (n1_1) -- (n1_3);
	\node[rectangle, fill=uzh!40, align=center] (n1_4) at (12+3.5, 7) {};
	\draw[-, line width=1pt] (n1_1) -- (n1_4);
	\node[rectangle, fill=uzh!40, align=center] (n1_5) at (10.5+3.5, 6) {};
	\draw[-, line width=1pt] (n1_3) -- (n1_5);
	\node[ellipse, fill=gray!40, align=center] (n1_6) at (11.5+3.5, 6) {};
	\draw[-, line width=1pt] (n1_3) -- (n1_6);
	\node[rectangle, fill=uzh!40, align=center] (n1_7) at (11+3.5, 5) {};
	\draw[-, line width=1pt] (n1_6) -- (n1_7);
	\node[rectangle, fill=uzh!40, align=center] (n1_8) at (12+3.5, 5) {};
	\draw[-, line width=1pt] (n1_6) -- (n1_8);
	
	\node[ellipse, fill=gray!40, align=center] (n2_0) at (12+6, 9) {};
	\node[ellipse, fill=gray!40, align=center] (n2_1) at (11.5+6, 8) {};
	\draw[-, line width=1pt] (n2_0) -- (n2_1);
	\node[rectangle, fill=uzh!40, align=center] (n2_2) at (12.5+6, 8) {};
	\draw[-, line width=1pt] (n2_0) -- (n2_2);
	\node[rectangle, fill=uzh!40, align=center] (n2_3) at (11+6, 7) {};
	\draw[-, line width=1pt] (n2_1) -- (n2_3);
	\node[rectangle, fill=uzh!40, align=center] (n2_4) at (12+6, 7) {};
	\draw[-, line width=1pt] (n2_1) -- (n2_4);
\end{tikzpicture}
}\\[2em]
\end{center}
\end{frame}


\begin{frame}{Personalised models}
Compute similarity (-weights) of patient $i$ to each patient $j$ in the training data:\\
How often are $i$ and $j$ assigned to the same subgroup?\\[2em]


\begin{tikzpicture}
	\node[ellipse, fill=gray!40, align=center] (n0) at (12, 9) {};
	\node[rectangle, fill=uzh!40, align=center] (n1) at (11.5, 8) {$i$};
	\draw[-, line width=1pt] (n0) -- (n1);
	\node[ellipse, fill=gray!40, align=center] (n2) at (12.5, 8) {};
	\draw[-, line width=1pt] (n0) -- (n2);
	\node[rectangle, fill=uzh!40, align=center] (n3) at (12, 7) {$j$};
	\draw[-, line width=1pt] (n2) -- (n3);
	\node[rectangle, fill=uzh!40, align=center] (n4) at (13, 7) {};
	\draw[-, line width=1pt] (n2) -- (n4);
	
	\node[ellipse, fill=gray!40, align=center] (n1_0) at (12+3.5, 9) {};
	\node[ellipse, fill=gray!40, align=center] (n1_1) at (11.5+3.5, 8) {};
	\draw[-, line width=1pt] (n1_0) -- (n1_1);
	\node[rectangle, fill=uzh!40, align=center] (n1_2) at (12.5+3.5, 8) {};
	\draw[-, line width=1pt] (n1_0) -- (n1_2);
	\node[ellipse, fill=gray!40, align=center] (n1_3) at (11+3.5, 7) {};
	\draw[-, line width=1pt] (n1_1) -- (n1_3);
	\node[rectangle, fill=uzh!40, align=center] (n1_4) at (12+3.5, 7) {$i, j$};
	\draw[-, line width=1pt] (n1_1) -- (n1_4);
	\node[rectangle, fill=uzh!40, align=center] (n1_5) at (10.5+3.5, 6) {};
	\draw[-, line width=1pt] (n1_3) -- (n1_5);
	\node[ellipse, fill=gray!40, align=center] (n1_6) at (11.5+3.5, 6) {};
	\draw[-, line width=1pt] (n1_3) -- (n1_6);
	\node[rectangle, fill=uzh!40, align=center] (n1_7) at (11+3.5, 5) {};
	\draw[-, line width=1pt] (n1_6) -- (n1_7);
	\node[rectangle, fill=uzh!40, align=center] (n1_8) at (12+3.5, 5) {};
	\draw[-, line width=1pt] (n1_6) -- (n1_8);
	
	\node[ellipse, fill=gray!40, align=center] (n2_0) at (12+6, 9) {};
	\node[ellipse, fill=gray!40, align=center] (n2_1) at (11.5+6, 8) {};
	\draw[-, line width=1pt] (n2_0) -- (n2_1);
	\node[rectangle, fill=uzh!40, align=center] (n2_2) at (12.5+6, 8) {};
	\draw[-, line width=1pt] (n2_0) -- (n2_2);
	\node[rectangle, fill=uzh!40, align=center] (n2_3) at (11+6, 7) {};
	\draw[-, line width=1pt] (n2_1) -- (n2_3);
	\node[rectangle, fill=uzh!40, align=center] (n2_4) at (12+6, 7) {$i, j$};
	\draw[-, line width=1pt] (n2_1) -- (n2_4);
\end{tikzpicture}
$\Rightarrow 2$ times
\end{frame}
 
 
\begin{frame}{Personalised models}
Compute the model using the weighted training data.\\
\textit{$\rightarrow$ observation $j$ enters 2 times in model for patient $i$.}
\end{frame}


\begin{frame}{ALSFRS}
  \includegraphics[width=0.5\textwidth]{results-dp1}
  \includegraphics[width=0.5\textwidth]{results-dp4}
\end{frame}

\begin{frame}{Survival time}
  \includegraphics[width=0.5\textwidth]{results-dp_surv1}
  \includegraphics[width=0.5\textwidth]{results-dp_surv3}
\end{frame}

 % 
 % <<testALSFRS, echo=FALSE, fig.show="hide">>=
 % load("data/ALSFRS_logLiks.rda")
 % load("data/ALSFRS_bootstrapLogLiks.rda")
 % 
 % llbs_diff <- bootstrapped.logliks$forest - bootstrapped.logliks$base_model
 % yy <- 0.0025 #0.01/4.62
 % lls <- data.frame(type = c("forest"),
 %                        label = c("H[1]"),
 %                        logLik = c(logLik_rf),
 %                        y = c(yy + 0.5/400))
 % lls$ll_diff <- lls$logLik - logLik_bmod
 % 
 % lls2 <- rbind(lls, lls)
 % lls2$yy <- lls2$y - c(Inf, 0.16/400)
 % 
 % p <- ggplot() +
 %   geom_line(aes(llbs_diff), stat = "density", trim = TRUE) +
 %   xlab("difference in log-likelihood between forest \n and base model")
 % 
 % p  +
 %   geom_line(data = lls2, aes(x = ll_diff, y = yy, group = type), linetype = 2) +
 %   geom_text(data = lls, aes(label = label, x = ll_diff, y = y), parse = TRUE, size = 6) 
 % 
 % p
 % @

\begin{frame}{Is the overall treatment effect good enough?}
\begin{description}
  \item[ALSFRS $H_0:$] Intercept and treatment effect are the same for all patients.\\
\end{description}
\begin{center} \scriptsize
  % Difference in log-likelihood between forest / personalised models and base model
  % on parametric bootstrap samples versus real data.\\
  \includegraphics[width=0.5\textwidth]{testALSFRS-1}
  \includegraphics[width=0.5\textwidth]{testALSFRS-2}\\
\end{center}
\end{frame}
%' 
%' <<testALSSurv, echo=FALSE, fig.show="hide">>=
%' # ALSsurv logliks
%' load("data/ALSsurv_logLiks.rda")
%' load("data/ALSsurv_bootstrapLogLiks.rda")
%' 
%' llbs_diff_surv <- bootstrapped.logliks_surv$forest - bootstrapped.logliks_surv$base_model
%' yy_surv <- 1
%' lls_surv <- data.frame(type = c("forest"),
%'                        label = c("H[1]"),
%'                        logLik = c(logLik_rf_surv),
%'                        y = c(yy_surv + 0.5))
%' lls_surv$ll_diff <- lls_surv$logLik - logLik_bmod_surv
%' 
%' lls2_surv <- rbind(lls_surv, lls_surv)
%' lls2_surv$yy <- lls2_surv$y - c(Inf, 0.16)
%' 
%' p <- ggplot() +
%'   geom_line(aes(llbs_diff_surv), stat = "density", trim = TRUE) +
%'   xlab("difference in log-likelihood between forest \n and base model") +
%'   ylim(0, 4.62)
%' 
%' p  +
%'   geom_line(data = lls2_surv, aes(x = ll_diff, y = yy, group = type), linetype = 2) +
%'   geom_text(data = lls_surv, aes(label = label, x = ll_diff, y = y), parse = TRUE, size = 6) 
%' 
%' p
%' @

\begin{frame}{Is the overall treatment effect good enough?}
\begin{description}
  \item[Survival time $H_0:$] Baseline hazard and treatment effect are the same for all patients.\\
\end{description}
\begin{center} \scriptsize
  % Difference in log-likelihood between forest / personalised models and base model
  % on parametric bootstrap samples versus real data.\\
  \includegraphics[width=0.5\textwidth]{testALSSurv-1}
  \includegraphics[width=0.5\textwidth]{testALSSurv-2}\\
\end{center}
\end{frame}


\begin{frame}{Summary}
\begin{itemize}
  \item Sound theoretical foundation for stratified and personalised medicine.
  \item Data-driven.\\[2em]
\end{itemize}
\visible<2>{
But
\begin{itemize}
  \item Results of secondary analysis need to be confirmed in independent trials.
\end{itemize}
}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%


\appendix
\section*{Bibliography}
\small
\thispagestyle{empty}
\begin{frame}{Literature}
  \begin{thebibliography}{9}
  \setbeamertemplate{bibliography item}[triangle]
      
    \bibitem{seibold_model-based_2016}
      H.~{Seibold},  A.~{Zeileis}, T.~{Hothorn}
      \newblock {\em Model-based Recursive Partitioning for Subgroup Analyses}
      \newblock International Journal of Biostatistics, 2016.
  
    \bibitem{seibold_individual_2016}
      H.~{Seibold},  A.~{Zeileis}, T.~{Hothorn}
      \newblock {\em Individual Treatment Effect Prediction for ALS Patients}
      \newblock ArXiv e-prints, 2016.

  \end{thebibliography}
\end{frame}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%

\backupbegin

\begin{frame}{Variable importance}
1. \underline{Tree log-likelihood}
\begin{itemize}
	\item Select the out-of-bag data $\mathcal{L}_t^c$ and determine the
	subgroup to which each observation $i$ belongs.
	\item Compute the log-likelihood contribution of each observation
	$i \in \mathcal{L}_t^c$ based on the respective model in the subgroup.
	\item Compute the out-of-bag log-likelihood as the sum of the
	contributions
	$$
		l_t =
		\sum\limits_{i \in \mathcal{L}_t^c} l((y, \mathbf{x})_i,
		\hat{\boldsymbol\vartheta}(\mathbf{z}_i)).
	$$
\end{itemize}

2. \underline{Variable importance}
$$
	\text{VI}_j = \frac{1}{T} \sum\limits_{t=1}^T
	\left[ l_t - l_t^{(j)}	\right]
$$
$l_t^{(j)}$ log-likelihood with variable $j$ permuted.
\end{frame}




\begin{frame}{Variable importance: ALSFRS}
\includegraphics[width=0.95\textwidth]{ALSFRS_varimp-1}

\end{frame}






\begin{frame}{Variable importance: Survival time}
\includegraphics[width=0.9\textwidth]{ALSsurv_varimp-1}
\end{frame}
\backupend





\end{document}
