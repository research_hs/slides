\documentclass[10pt]{beamer}\usepackage[]{graphicx}\usepackage[]{color}
%% maxwidth is the original width if it is less than linewidth
%% otherwise use linewidth (to make sure the graphics do not exceed the margin)
\makeatletter
\def\maxwidth{ %
  \ifdim\Gin@nat@width>\linewidth
    \linewidth
  \else
    \Gin@nat@width
  \fi
}
\makeatother

\definecolor{fgcolor}{rgb}{0.345, 0.345, 0.345}
\newcommand{\hlnum}[1]{\textcolor[rgb]{0.686,0.059,0.569}{#1}}%
\newcommand{\hlstr}[1]{\textcolor[rgb]{0.192,0.494,0.8}{#1}}%
\newcommand{\hlcom}[1]{\textcolor[rgb]{0.678,0.584,0.686}{\textit{#1}}}%
\newcommand{\hlopt}[1]{\textcolor[rgb]{0,0,0}{#1}}%
\newcommand{\hlstd}[1]{\textcolor[rgb]{0.345,0.345,0.345}{#1}}%
\newcommand{\hlkwa}[1]{\textcolor[rgb]{0.161,0.373,0.58}{\textbf{#1}}}%
\newcommand{\hlkwb}[1]{\textcolor[rgb]{0.69,0.353,0.396}{#1}}%
\newcommand{\hlkwc}[1]{\textcolor[rgb]{0.333,0.667,0.333}{#1}}%
\newcommand{\hlkwd}[1]{\textcolor[rgb]{0.737,0.353,0.396}{\textbf{#1}}}%
\let\hlipl\hlkwb

\usepackage{framed}
\makeatletter
\newenvironment{kframe}{%
 \def\at@end@of@kframe{}%
 \ifinner\ifhmode%
  \def\at@end@of@kframe{\end{minipage}}%
  \begin{minipage}{\columnwidth}%
 \fi\fi%
 \def\FrameCommand##1{\hskip\@totalleftmargin \hskip-\fboxsep
 \colorbox{shadecolor}{##1}\hskip-\fboxsep
     % There is no \\@totalrightmargin, so:
     \hskip-\linewidth \hskip-\@totalleftmargin \hskip\columnwidth}%
 \MakeFramed {\advance\hsize-\width
   \@totalleftmargin\z@ \linewidth\hsize
   \@setminipage}}%
 {\par\unskip\endMakeFramed%
 \at@end@of@kframe}
\makeatother

\definecolor{shadecolor}{rgb}{.97, .97, .97}
\definecolor{messagecolor}{rgb}{0, 0, 0}
\definecolor{warningcolor}{rgb}{1, 0, 1}
\definecolor{errorcolor}{rgb}{1, 0, 0}
\newenvironment{knitrout}{}{} % an empty environment to be redefined in TeX

\usepackage{alltt}

% knit from command line with R CMD Sweave
%\VignetteEngine{knitr::knitr}

\input{./input/head}
\input{./input/commands}

%% above frame title 
\addheadboxtemplate{\color[rgb]{1,1,1}}{
  \color{uzh} \underline{{\hspace{1em}
  \includegraphics[height=3.3em]{palm_small}\includegraphics[height=3.3em]{palm_small}\includegraphics[height=3.3em]{palm_small}
  \hspace{0.75\paperwidth}
  \color{black} \tiny Heidi Seibold}
  \hspace{0.1\paperwidth}}
}


%% figures path
\graphicspath{{figure/}}

%% Title
\author{Heidi Seibold}
\title{Generalised Linear Model Trees with Global
Additive Effects -- PALM trees\\[1em]}
\subtitle{My PhD Topic:\\
Model-Based Recursive Partitioning for Stratified and Personalised Medicine}
\date{30.11.2016}
\institute[]{%Epidemiology, Biostatistics and Prevention Institute\\[1em] 
\includegraphics[width=0.3\textwidth]{uzh_logo_e_pos}\\[1em]
% Joint work with Torsten Hothorn (UZH) and Achim Zeileis (UIBK), sponsored by SNF
}
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\begin{document}

\setbeamercolor{bgr}{fg=black,bg=uzh}

\thispagestyle{empty}
\begin{frame}
\transsplithorizontalout
%\vspace*{1,5cm}
\titlepage
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%

\begin{frame}{What is \textit{Stratified Medicine}?}
\vspace*{3em}
\textbf{Stratified medicine}:\\
Find subgroups of patients that differ with respect to their treatment effect.
Estimate the treatment effect for each subgroup.\\[3em]
\begin{center}
\begin{tikzpicture}
	\node[ellipse, fill=gray!40, align=center] (n0) at (12, 9) {};
	\node[rectangle, fill=uzh!40, align=center] (n1) at (11.5, 8) {};
	\draw[-, line width=1pt] (n0) -- (n1);
	\node[ellipse, fill=gray!40, align=center] (n2) at (12.5, 8) {};
	\draw[-, line width=1pt] (n0) -- (n2);
	\node[rectangle, fill=uzh!40, align=center] (n3) at (12, 7) {};
	\draw[-, line width=1pt] (n2) -- (n3);
	\node[rectangle, fill=uzh!40, align=center] (n4) at (13, 7) {};
	\draw[-, line width=1pt] (n2) -- (n4);
\end{tikzpicture}\\
\end{center}
\end{frame}



\begin{frame}{Predictive and prognostic factors}

{
\scriptsize
\begin{tikzpicture}
\node[circle, fill=gray!20, text width=1.6cm, align=center] (prog) at (12-6,9) {prognostic factor $z$};
\node[circle, fill=uzh!40, text width=1.2cm, align=center] (outc) at (15-6,9) {primary endpoint};
\draw[->, line width=2pt] (prog) -- node [midway, above] {} (outc);

\node[circle, fill=gray!20, text width=1.6cm, align=center] (pred) at (6+6,9) {predictive factor $z$};
\node[circle, fill=gray!20, text width=1.2cm, align=center] (treat) at (8.7+6,10.2) {treatment};
\node[circle, fill=gray!20, text width=1.2cm, align=center] (outc2) at (8.7+6,7.8) {primary endpoint};
\draw[->, line width=2pt] (pred) -- node [midway, above] {} (8.5+6,9);
\draw[->, line width=4pt, uzh!50] (treat) -- (outc2);
\end{tikzpicture}
}

\end{frame}

%-------------------------------------------------------------------------%



% \begin{frame}{Stratified medicine: Subgroup analyses}
% \begin{itemize}
%   \item Find subgroups of patients with similar treatment effects 
%   \item ``Believe'' in small number of subgroups
%   \item Use automatic procedure to identify predictive factors and subgroups
%   \item Easy to interpret
% \end{itemize}
% 
% \begin{center}
% \begin{tikzpicture}
% 	\node[ellipse, fill=gray!40, align=center] (n0) at (12, 9) {};
% 	\node[rectangle, fill=uzh!40, align=center] (n1) at (11.5, 8) {};
% 	\draw[-, line width=1pt] (n0) -- (n1);
% 	\node[ellipse, fill=gray!40, align=center] (n2) at (12.5, 8) {};
% 	\draw[-, line width=1pt] (n0) -- (n2);
% 	\node[rectangle, fill=uzh!40, align=center] (n3) at (12, 7) {};
% 	\draw[-, line width=1pt] (n2) -- (n3);
% 	\node[rectangle, fill=uzh!40, align=center] (n4) at (13, 7) {};
% 	\draw[-, line width=1pt] (n2) -- (n4);
% \end{tikzpicture}\\
% \end{center}
% \end{frame}

%-------------------------------------------------------------------------%

\begin{frame}{Model-based recursive partitioning}
Start with model $g(\boldsymbol{\mu}) = \mathbf{x}^\top \boldsymbol{\beta}$ with 

$$\boldsymbol{\beta} = 
\begin{pmatrix}
\text{intercept}\\
\text{treatment effect}\\
\text{\color{gray} effects of known prognostic factors}
\end{pmatrix}
$$
\vspace*{2em}\\
$\rightarrow$ Take model from study protocol.
\end{frame}



\begin{frame}{Model-based recursive partitioning}

\begin{itemize}
\item[$\Rightarrow$] Find partitions $\{\segb_b\}$ ($b=1,...,B$) based on patient characteristics $\theoz = (\theoz_1,...,\theoz_J) \in \spacz$
\item[$\Rightarrow$] Fit separate models $g(\boldsymbol{\mu}) = \mathbf{x}^\top \boldsymbol{\beta}_b$ in partitions.\\
\end{itemize}\vspace{1em}
{
\scriptsize
\center
\begin{tikzpicture}
	\node[ellipse, fill=gray!40, align=center, text width=.7em, text height=.7em] (n0) at (12, 10) {};
	\node[rectangle, fill=uzh!40, align=center] (n1) at (10, 7) {$\boldsymbol{\beta}_1$};
	\draw[-, line width=1pt] (n0) -- (n1) node[midway, left] {};
	\node[ellipse, fill=gray!40, align=center, text width=.7em, text height=.7em] (n2) at (13, 8.5) {};
	\draw[-, line width=1pt] (n0) -- (n2) node[midway, right] {};
	\node[rectangle, fill=uzh!40, align=center] (n3) at (12, 7) {$\boldsymbol{\beta}_2$};
	\draw[-, line width=1pt] (n2) -- (n3) node[midway, left] {};
	\node[rectangle, fill=uzh!40, align=center] (n4) at (14, 7) {$\boldsymbol{\beta}_3$};
	\draw[-, line width=1pt] (n2) -- (n4) node[midway, right] {};
\end{tikzpicture}\\
}
\end{frame}


\begin{frame}{How to find the Partitions?}
Test of independence between the \textbf{partial score functions} and each \textbf{patient characteristic}:
\begin{align*}
H_0^{\beta_0,j}&: \quad \psi_{\beta_0}(\theodat, \hat{\boldsymbol{\beta}}) \quad\bot\quad \theoz_j\\
H_0^{\beta_\text{trt},j}&: \quad \psi_{\beta_\text{trt}}(\theodat, \hat{\boldsymbol{\beta}}) \quad\bot\quad \theoz_j, \quad j = 1, \dots, J\\
& \vdots
\end{align*}\\[1em]
$\psi_{\beta_0}, \psi_{\beta_\text{trt}}$ partial derivatives of the log-likelihood with respect to $\beta_0 / \beta_\text{trt}$.\\[1.5em]
\begin{itemize}
\item Partition if global p-value smaller than significance level
\item Use as split variable the one with the smallest p-value
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%

\begin{frame}{Evolution of the PALM tree}
\begin{columns}
\column{.4\textwidth}
\includegraphics[width = \textwidth]{evolution.png}\footnotemark

\column{.6\textwidth}
\begin{align*}
    \text{GLM} \quad & g(\boldsymbol{\mu}) = \mathbf{x}^\top 
            \boldsymbol{\beta} \\
    \text{GLM tree} \quad & g(\boldsymbol{\mu}) = \mathbf{x}^\top 
            \boldsymbol{\beta}(\mathbf{z})\\ 
    \text{PALM tree} \quad & g(\boldsymbol{\mu}) = \mathbf{x}_V^\top 
            \boldsymbol{\beta}(\mathbf{z}) + 
            \mathbf{x}_F^\top \boldsymbol{\gamma} \\[1em]
\end{align*}
\end{columns}

\footnotetext{\scriptsize\url{commons.wikimedia.org}}
\end{frame}

%-------------------------------------------------------------------------%


% \begin{frame}{GLM tree and PALM tree}
% \begin{columns}
% \column{.4\textwidth}
% 
% {
% \small
% \center
% \begin{tikzpicture}
% 	\node[ellipse, fill=gray!40, align=center] (n0) at (12, 10) {$z_1 \leq 0$};
% 	\node[rectangle, fill=uzh!40, align=center] (n1) at (10, 7) {$\beta_1$};
% 	\draw[-, line width=1pt] (n0) -- (n1) node[midway, left] {Yes};
% 	\node[ellipse, fill=gray!40, align=center] (n2) at (13, 8.5) {$z_2 \leq 0$};
% 	\draw[-, line width=1pt] (n0) -- (n2) node[midway, right] {No};
% 	\node[rectangle, fill=uzh!40, align=center] (n3) at (12, 7) {$\beta_2$};
% 	\draw[-, line width=1pt] (n2) -- (n3) node[midway, left] {Yes};
% 	\node[rectangle, fill=uzh!40, align=center] (n4) at (14, 7) {$\beta_3$};
% 	\draw[-, line width=1pt] (n2) -- (n4) node[midway, right] {No};
% \end{tikzpicture}\\
% }
% 
% \column{.6\textwidth}
%   \begin{align*}
%       \text{GLM tree} \quad & g(\boldsymbol{\mu}) = \mathbf{x}^\top 
%               \boldsymbol{\beta}(\mathbf{z})\\ 
%       \text{PALM tree} \quad & g(\boldsymbol{\mu}) = \mathbf{x}_V^\top 
%               \boldsymbol{\beta}(\mathbf{z}) + 
%               \mathbf{x}_F^\top \boldsymbol{\gamma} \\[1em]
%   \end{align*}
%   \begin{align*}
%         \beta(\mathbf{z}) &= \begin{cases}
%           \boldsymbol{\beta}_1 & \quad \text{if } 
%                 z_1 \leq 0 \\
%           \boldsymbol{\beta}_2 & \quad \text{if } 
%                 (z_1 > 0) ~\wedge~ (z_2 \leq 0) \\
%           \boldsymbol{\beta}_3 & \quad \text{if }  
%                 (z_1 > 0) ~\wedge~ (z_2 > 0)
%         \end{cases}
%   \end{align*}
% 
% \end{columns}
% \end{frame}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%
\begin{frame}{Math Exam PALM tree}
  \begin{description}[align=left]
    \item[Outcome:] percentage of correct answers in the exam\\[1em]
    \item[``Treatment'':] early morning versus late morning group\\[1em]
    \item[Known prognostic factor:] percentage of successful online tests\\[1em]
    \item[Student characteristics:] 
  \end{description}
  \vspace{-0.7em}
     \begin{itemize}
      \item \textit{gender}
      \item number of \textit{semesters} the student has already been studying
      \item number of times the student has already \textit{attempted} the exam
      \item type of \textit{study} (bachelor / diploma)
      \item percentage of successful online \textit{tests}
     \end{itemize}
\end{frame}






\begin{frame}%{Math Exam PALM tree}
\hspace*{-3em}
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}
\includegraphics[width=1.15\textwidth]{figure/mathp1-1} 

\end{knitrout}

  % \hspace*{-3em}\includegraphics[width = 1.2\textwidth]{figure/palm-1.pdf}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%


\section*{Bibliography}
\small
\thispagestyle{empty}
% \usebackgroundtemplate{\tikz\node[opacity=0.2]{\includegraphics[height=\paperheight]{palm}};}
\begin{frame}{Literature}
\begin{columns}
\column{.8\textwidth}
  \begin{thebibliography}{9}
  \setbeamertemplate{bibliography item}[triangle]
  
    \bibitem{seibold_individual_2016}
      H.~{Seibold}, T.~{Hothorn}, A.~{Zeileis}
      \newblock {\em Generalised Linear Model Trees with Global Additive Effects}
      % \newblock ArXiv e-prints, 2016.
      
    \bibitem{seibold_model-based_2016}
      H.~{Seibold},  A.~{Zeileis}, T.~{Hothorn}
      \newblock {\em Model-based Recursive Partitioning for Subgroup Analyses}
      \newblock International Journal of Biostatistics, 2016.
  \end{thebibliography}

\column{.1\textwidth}
  \hspace*{-2em}
  \includegraphics[height=0.8\textheight, trim={20em 0 25em 0}, clip]{palm}\footnotemark

\end{columns}
\footnotetext{\scriptsize\url{www.publicdomainpictures.net}}
\end{frame}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%

\backupbegin
\begin{frame}[fragile]{}
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}
\includegraphics[width=\maxwidth]{figure/math_mods-1} 

\end{knitrout}
\end{frame}


\begin{frame}{Simulation study}
\small
\vspace{.5em}
\begin{tabular}{|cl|}
\hline
&\\
& $\mathbf{x}_A$ randomized treatment\\
\large \hspace{1.2em} $ 
\mathbf{y} = \mathbf{x}_A^\top \beta(\mathbf{z}) + 
          \mathbf{x}_F^\top \boldsymbol{\gamma} +
          \boldsymbol{\epsilon} 
$ \hspace{1.2em}
& $\mathbf{z}$ patient characteristics\\
& $\mathbf{x}_F$ known prognostic factors\\
&\\
\hline
\end{tabular}\\[.5em]

Simulate $\mathbf{Z} \sim \mathcal{N}_{30}(\mathbf{0}, \boldsymbol{\Sigma})$ with correlation $0.2$.\\
\begin{tabular}{ll}
$z_1, z_2$& true predictive factors\\ 
$z_3, z_4$& known prognostic factors $\mathbf{x}_F = (z_3, z_4)^\top$\\
$z_5, \dots, z_{30}$& noise variables.
\end{tabular}

\vspace{1.5em}

\begin{columns}
\column{.45\textwidth}
\begin{center}
\resizebox{0.8\textwidth}{!}{ \scriptsize
  \begin{tikzpicture}
  
        \node[ellipse, draw, align=center] (n0) at (12, 10) {$z_1 \leq 0$};
        \node[rectangle, draw, align=center] (n1) at (10, 7)
                {$\boldsymbol{\beta}_1$};
        \draw[-, line width=1pt] (n0) -- (n1) node[midway, left] {Yes};
        \node[ellipse, draw, align=center] (n2) at (13, 8.5) {$z_2 \leq 0$};
        \draw[-, line width=1pt] (n0) -- (n2) node[midway, right] {No};
        \node[rectangle, draw, align=center] (n3) at (12, 7)
                {$\boldsymbol{\beta}_2$};
        \draw[-, line width=1pt] (n2) -- (n3) node[midway, left] {Yes};
        \node[rectangle, draw, align=center] (n4) at (14, 7)
                {$\boldsymbol{\beta}_3$};
        \draw[-, line width=1pt] (n2) -- (n4) node[midway, right] {No};
  \end{tikzpicture}
}
\end{center}
\column{.55\textwidth}
\begin{align*}
        \beta(\mathbf{z}) &= \left\{ \begin{array}{ll}
           \beta_1 & \text{if } z_1 \leq 0 \\
           \beta_2 = \beta_1 + \Delta_\beta & 
                \text{if } z_1 > 0 ~\wedge~ z_2 \leq 0 \\
           \beta_3 = \beta_2 + \Delta_\beta & 
                \text{if } z_1 > 0 ~\wedge~ z_2 > 0.
        \end{array} \right.
\end{align*}
\vspace{3em}
\end{columns}
\end{frame}


\begin{frame}{Simulation study}

\begin{description}
        \item[PALM tree] $\boldsymbol{\mu} = \mathbf{x}_V^\top 
            \boldsymbol{\beta}(\mathbf{z}) + 
            \mathbf{x}_F^\top \boldsymbol{\gamma}$ 
            with\\ 
            $\mathbf{x_V} = (\mathbf{1}, \mathbf{x}_A)$ and
            $\mathbf{x}_F = (z_3, z_4)$. \\[1em]
        \item[LM tree 1] $\boldsymbol{\mu} = \mathbf{x}^\top 
            \boldsymbol{\beta}(\mathbf{z})$ 
            with $\mathbf{x} = (\mathbf{1}, \mathbf{x}_A)$. \\[1em] 
        \item[LM tree 2] $\boldsymbol{\mu} = \mathbf{x}^\top 
            \boldsymbol{\beta}(\mathbf{z})$ 
            with $\mathbf{x} = (\mathbf{1}, \mathbf{x}_A, z_3, z_4)$. \\[1em] 
	\item[OTR\footnote{\tiny Zhang B, Tsiatis AA, Davidian M, Zhang M,
	Laber E	(2012). ``Estimating Optimal Treatment Regimes from a
	Classification Perspective.'' Stat, 1(1), 103-114.
	doi:10.1002/sta.411.}] 
	    with outcome model $\boldsymbol{\mu} = (\mathbf{1},
            \mathbf{x}_A, z_3, z_4)^\top \boldsymbol{\gamma} + (\mathbf{x}_A
            \circ \mathbf{z})^\top \boldsymbol{\beta}$,\\
	    new outcome $I(\hat{\mu}_1 - \hat{\mu}_0 > 0)$,\\
	    and pruned and weighted ($|\hat{\mu}_1 - \hat{\mu}_0|$) CARTs
            as classification method. 
\end{description}

\end{frame}



\begin{frame}{Simulation study}
Evaluation via Adjusted Rand Index (ARI)\\[3em]
How well do the retrieved subgroups agree with the true subgroups?\\[2em] 
Subgroups found $=$ true subgroups $~ \Rightarrow$ ARI $ = 1$\\[0.5em]
Random group assignment $~ \Rightarrow$ ARI $ = 0$
\end{frame}


\begin{frame}[fragile]{Simulation study}
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}
\includegraphics[width=\textwidth]{figure/palm-1} 

\end{knitrout}
\footnotesize More simulation scenarios and measures in the paper.
\end{frame}



\backupend





\end{document}
