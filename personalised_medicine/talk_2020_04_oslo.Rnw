\documentclass[12pt,aspectratio=169]{beamer}


% gentsroom pics
\usepackage{marvosym}
\definecolor{ccpk}{HTML}{cc00cc}
\definecolor{cctq}{rgb}{0, 204, 255}
\definecolor{ccgr}{HTML}{00cc66}
\definecolor{ccor}{rgb}{230, 138, 0}

% knit from command line with R CMD Sweave
%\VignetteEngine{knitr::knitr}

\input{./input/head}
% \input{./input/commands}

%% above frame title
% \addheadboxtemplate{\color[rgb]{1,1,1}}{\color{lmu} \underline{{\hspace{3pt}\includegraphics[width=0.15\textheight]{lmu_logo_gr}
% \hspace{0.01\paperwidth}
% \color{black} \tiny Model-based trees and random forests}
% \hspace{0.48\paperwidth}
% \color{black} \tiny Heidi Seibold
% \hspace{0.1\paperwidth}
% }}

%% figures path
\graphicspath{{figure/}}

%% Title
\author{Heidi Seibold\\[1.5em] \includegraphics[width=0.17\textwidth]{lmu_logo}}
\title{Model-based trees and random forests for personalised treatment effect estimation}
\date{5.5.2020}
\institute[]{Joint work with Torsten Hothorn (UZH) and Achim Zeileis (UIBK)}


\begin{document}
<<dsetup, echo=FALSE>>=
library("knitr")
knitr::opts_chunk$set(cache = TRUE)
library("survival")
library("ggplot2")
theme_set(theme_classic(base_size = 18))
library("gridExtra")

library("xtable")

## download data
download.file(url = "https://r-forge.r-project.org/scm/viewvc.php/*checkout*/pkg/TH.data/inst/rda/ALSsurvdata.rda?revision=41&root=thdata", destfile = "data/ALSsurvdata.rda")
@



\setbeamercolor{bgr}{fg=black,bg=lmu}

\thispagestyle{empty}
\begin{frame}
\transsplithorizontalout
\titlepage
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%

\begin{frame}{}
\begin{center}
\includegraphics[height=\textheight]{drug_decision}
\end{center}
\end{frame}

\begin{frame}{Stratified and Personalised Treatment Effects}

\textbf{Overall/average treatment effect}\\[0.5em]
{ \huge
\textcolor{uzh}{
\Ladiesroom \Gentsroom \Gentsroom \Ladiesroom \Gentsroom \Ladiesroom \Ladiesroom
\Ladiesroom \Gentsroom \Gentsroom \Ladiesroom \Gentsroom \Ladiesroom \Ladiesroom
}
}
\\[2em]

\textbf{Stratified treatment effect}\\[0.5em]
{ \huge
\textcolor{uzh}{
\Ladiesroom \Gentsroom \Gentsroom \Ladiesroom \Gentsroom
}
\textcolor{lmu}{
\Gentsroom \Gentsroom \Ladiesroom \Ladiesroom \Gentsroom
}
\textcolor{red}{
\Ladiesroom \Ladiesroom \Gentsroom \Ladiesroom \Gentsroom
}
}
\\[2em]

\textbf{Personalised treatment effect}\\[0.5em]
{\huge
\textcolor{ccpk}{\Ladiesroom}
\textcolor{uzh}{\Gentsroom}
\textcolor{blue}{\Ladiesroom}
\textcolor{cctq}{\Gentsroom}
\textcolor{lmu}{\Ladiesroom}
\textcolor{ccgr}{\Gentsroom}
\textcolor{ccor}{\Ladiesroom}
\textcolor{red}{\Ladiesroom}
}
\end{frame}

\begin{frame}[fragile]{Overall Treatment Effect}
\begin{center}
{\Large
\color{uzh}
\Ladiesroom \Gentsroom \Gentsroom \Ladiesroom \Gentsroom \Ladiesroom \Ladiesroom
\Ladiesroom \Gentsroom \Gentsroom \Ladiesroom \Gentsroom \Ladiesroom \Ladiesroom\\[.5em]
}
\end{center}
<<eval=FALSE>>=
base_model <- model(endpoint ~ treatment, data)
@
\end{frame}



\begin{frame}[fragile]{Stratified Treatment Effects}
\begin{center}
  \resizebox{0.5\textwidth}{!}{
\begin{tikzpicture}
  \node[ellipse, fill=gray!40, align=center] (n0) at (12, 9) {};
	\node[rectangle, fill=lmu!20, align=center] (n1) at (10, 7.4) {\textcolor{lmu}{\Ladiesroom \Gentsroom \Gentsroom \Ladiesroom \Gentsroom}};
	\draw[-, line width=1pt] (n0) -- (n1);
	\node[ellipse, fill=gray!40, align=center] (n2) at (13, 8.2) {};
	\draw[-, line width=1pt] (n0) -- (n2);
	\node[rectangle, fill=uzh!20, align=center] (n3) at (12, 7.4) {\textcolor{uzh}{\Gentsroom \Gentsroom \Ladiesroom \Ladiesroom \Gentsroom}};
	\draw[-, line width=1pt] (n2) -- (n3);
	\node[rectangle, fill=red!20, align=center] (n4) at (14, 7.4) {\textcolor{red}{\Ladiesroom \Ladiesroom \Gentsroom \Ladiesroom \Gentsroom}};
	\draw[-, line width=1pt] (n2) -- (n4);
\end{tikzpicture}
}\\[2em]
\end{center}
Patient $\textcolor{lmu}{\Large\text{\Ladiesroom}_i}$ with characteristics $\textcolor{lmu}{z_i}$ $\rightarrow$ subgroup \textcolor{lmu}{1}:\\
<<mbmodel, eval=FALSE>>=
model_sg1 <- model(endpoint ~ treatment, data,
                   weights = (subgroup == 1))
@
\end{frame}



\begin{frame}{Predictive and prognostic factors}
\begin{center}
  \resizebox{0.5\textwidth}{!}{
\begin{tikzpicture}
  \node[ellipse, fill=gray!40, align=center] (n0) at (12, 9) {};
	\node[rectangle, fill=lmu!20, align=center] (n1) at (10, 7.4) {\textcolor{lmu}{\Ladiesroom \Gentsroom \Gentsroom \Ladiesroom \Gentsroom}};
	\draw[-, line width=1pt] (n0) -- (n1);
	\node[ellipse, fill=gray!40, align=center] (n2) at (13, 8.2) {};
	\draw[-, line width=1pt] (n0) -- (n2);
	\node[rectangle, fill=uzh!20, align=center] (n3) at (12, 7.4) {\textcolor{uzh}{\Gentsroom \Gentsroom \Ladiesroom \Ladiesroom \Gentsroom}};
	\draw[-, line width=1pt] (n2) -- (n3);
	\node[rectangle, fill=red!20, align=center] (n4) at (14, 7.4) {\textcolor{red}{\Ladiesroom \Ladiesroom \Gentsroom \Ladiesroom \Gentsroom}};
	\draw[-, line width=1pt] (n2) -- (n4);
\end{tikzpicture}
}\\[1.5em]
\resizebox{0.7\textwidth}{!}{
\scriptsize
\begin{tikzpicture}
\node[circle, fill=gray!20, text width=1.6cm, align=center] (prog) at (12-6,9) {prognostic factor $z$};
\node[circle, fill=lmu!70, text width=1.2cm, align=center] (outc) at (15-6,9) {endpoint};
\draw[->, line width=2pt] (prog) -- node [midway, above] {} (outc);

\node[circle, fill=gray!20, text width=1.6cm, align=center] (pred) at (6+6,9) {predictive factor $z$};
\node[circle, draw=black, text width=1.2cm, align=center] (treat) at (8.7+6,10.2) {treatment};
\node[circle, draw=black, text width=1.2cm, align=center] (outc2) at (8.7+6,7.8) {endpoint};
\draw[->, line width=2pt] (pred) -- node [midway, above] {} (8.5+6,9);
\draw[->, line width=4pt, lmu] (treat) -- (outc2);
\end{tikzpicture}
}

\end{center}
\end{frame}


\begin{frame}[fragile]{How to find the Partitions?}

{
\small
Test of independence between the elements of the \textbf{score function} and each \textbf{patient characteristic} $z_1, \dots, z_C$:

<<dscores, echo=FALSE, fig.height=2.4>>=
library("sandwich")
set.seed(225)

## get legend
# https://github.com/hadley/ggplot2/wiki/Share-a-legend-between-two-ggplot2-graphs
g_legend <- function(a.gplot){
  tmp <- ggplot_gtable(ggplot_build(a.gplot))
  leg <- which(sapply(tmp$grobs, function(x) x$name) == "guide-box")
  legend <- tmp$grobs[[leg]]
  return(legend)
}

## data
d <- factor(sample(0:1, 200, replace = TRUE))
z <- rnorm(200)
y <- 1.9 + 0.2 * I(d == 1) + 1.8 * I(z < 0) + 3.6 * I((z > 0) & (d == 1)) + rnorm(200, 0, 0.7)
data <- data.frame(y, d, z)
data$d <- factor(data$d, labels = c("A", "B"))

## model
mod <- lm(y ~ d)
estf <- as.data.frame(estfun(mod))
names(estf) <- c("s_0", "s_1")
pd <- cbind(data, estf)

## plots
p <- ggplot(pd, aes(y = s_0, x = z)) + geom_point(aes(shape = d)) +  theme_bw() +
  geom_hline(yintercept = 0, colour = "grey", size = 1, alpha = 0.5) + ylim(-3.1, 3.1) +
  guides(colour=FALSE) + ylab(expression(s[alpha])) + xlab(expression(z[1])) +
  scale_shape_discrete(solid = FALSE) + theme(legend.title=element_blank()) 
mylegend <- g_legend(p)
p <- p + theme(legend.position="none")

q <- ggplot(pd, aes(y = s_1, x = z)) + geom_point(aes(shape = d)) + theme_bw() + 
  geom_hline(yintercept = 0, colour = "grey", size = 1, alpha = 0.5) + ylim(-3.1, 3.1) +
  guides(colour=FALSE) + ylab(expression(s[beta])) + xlab(expression(z[1])) +
  scale_shape_discrete(solid = FALSE) + theme(legend.position="none")

grid.arrange(arrangeGrob(p, q, ncol = 2), 
             mylegend, ncol = 2, widths = c(10, 1))
@
\begin{itemize}
\item Partition if global p-value smaller than significance level
\item Use as split variable the one with the smallest p-value
\end{itemize}
}

\end{frame}

%-------------------------------------------------------------------------%


\begin{frame}{PRO-ACT database\footnote{\url{https://nctu.partners.org/ProACT}}}
\begin{itemize}
\item Disease: ALS
\item 23 clinical trials
\item Riluzole versus no treatment
\item Endpoint:	Survival time
\end{itemize}
\end{frame}

<<dALSsurv_mod, echo=FALSE, warning=FALSE, message=FALSE>>=
library("model4you")
load("data/ALSsurvdata.rda")
@

<<diffms, echo=FALSE>>=
diffms <- function(model, return_medians = FALSE) {
  ## model coefficients
  coefs <- c(coef(model), scale = model$scale)

  ## difference in median survival
  p = 0.5
  m0 <- qweibull(p = p, shape = 1/coefs["scale"],
    scale = exp(coefs["(Intercept)"]))
  m1 <- qweibull(p = p, shape = 1/coefs["scale"],
    scale = exp(coefs["(Intercept)"] + coefs[2]))
  median_sdiff <- m1 - m0
  if(return_medians) median_sdiff <- c(median_sdiff, m0, m1)

  return(matrix(median_sdiff))
}
@

\begin{frame}[fragile]{PRO-ACT Overall Treatment Effect}
<<bm, message=FALSE, fig.height=3.5, out.width="0.7\\textwidth">>=
bmod <- survreg(Surv(survival.time, cens) ~ Riluzole,
  data = ALSsurvdata)
@

<<bm_plot, message=FALSE, fig.height=3.5, out.width="0.7\\textwidth", echo=FALSE>>=
p <- survreg_plot(bmod, yrange = c(0, 1000))

diffms_bmod <- diffms(bmod, return_medians = TRUE)
ddiffms <- data.frame(y = 0.5, medians = diffms_bmod[2:3, 1])
p + geom_segment(y = 0.5, yend = 0.5, 
    x = diffms_bmod[2, 1], xend = diffms_bmod[3, 1],
    color = 1) +
    geom_text(y = 0.5, x = 300, 
        label = paste("Difference in median survival:\n",
            round(diffms_bmod[1, 1]), "days"),
        color = 1)

@

\end{frame}

\begin{frame}[fragile]{PRO-ACT Stratified Treatment Effect}
<<tr, fig.height=5, fig.width=11>>=
tree <- pmtree(bmod, data = ALSsurvdata,
  control = ctree_control(maxdepth = 2))
@

<<tr_plot, fig.height=4.5, fig.width=11, echo=FALSE>>=
# plot(tree, terminal_panel = node_pmterminal(tree,
#   coeftable = FALSE, plotfun = survreg_plot, yrange = c(0, 1000),
#   confint = FALSE), gp = gpar(cex = 0.9))

pl <-  function(mod, data = NULL, theme = theme_classic(), yrange = NULL) {
  survreg_plot(mod, data = data, theme = theme, yrange = yrange) + theme(legend.position = "none")
  }

plot(tree, terminal_panel = node_pmterminal(tree,
  coeftable = FALSE, plotfun = pl, yrange = c(0, 1000),
  confint = FALSE), gp = gpar(cex = 0.9), pop = FALSE)
pushViewport(viewport(x = 0.9, y = 0.9, width = 0.3, height = 0.3))
grid.lines(x = c(0, 0.1), y = c(0.55, 0.55), gp = gpar(col = "#F8766D"))
grid.lines(x = c(0, 0.1), y = c(0.4, 0.4), gp = gpar(col = "#00BFC4"))
grid.text(x = c(0.15), y = c(0.55), "No Riluzole", just = "left")
grid.text(x = c(0.15), y = c(0.4), "Riluzole", just = "left")
@

\end{frame}



%-------------------------------------------------------------------------%
%-------------------------------------------------------------------------%

\begin{frame}[fragile]{Personalised Treatment Effects}
\begin{center}
{\huge
\textcolor{ccpk}{\Ladiesroom}
\textcolor{uzh}{\Gentsroom}
\textcolor{blue}{\Ladiesroom}
\textcolor{cctq}{\Gentsroom}
\textcolor{lmu}{\Ladiesroom}
\textcolor{ccgr}{\Gentsroom}
\textcolor{ccor}{\Ladiesroom}
\textcolor{red}{\Ladiesroom}
}\\[2em]
\end{center}
Patient $\textcolor{ccpk}{\Large\text{\Ladiesroom}_i}$ with characteristics $\textcolor{ccpk}{z_i}$:\\[0.5em]
<<pmodel, eval=FALSE>>=
model_i <- model(endpoint ~ treatment, data,
                 weights = w_i)
@

\end{frame}

\begin{frame}[fragile]{How to get the weights for \textcolor{ccpk}{\huge$\text{\Ladiesroom}_{i}$} ?}
\vspace{.5em}
\begin{center}
  \begin{tikzpicture}
  	\node[ellipse, fill=gray!40, align=center] (n0) at (12, 9) {};
  	\node[rectangle, fill=lmu!40, align=center] (n1) at (11.5, 8) {$i$};
  	\draw[-, line width=1pt] (n0) -- (n1);
  	\node[ellipse, fill=gray!40, align=center] (n2) at (12.5, 8) {};
  	\draw[-, line width=1pt] (n0) -- (n2);
  	\node[rectangle, fill=lmu!40, align=center] (n3) at (12, 7) {$j$};
  	\draw[-, line width=1pt] (n2) -- (n3);
  	\node[rectangle, fill=lmu!40, align=center] (n4) at (13, 7) {$k$};
  	\draw[-, line width=1pt] (n2) -- (n4);

  	\node[ellipse, fill=gray!40, align=center] (n1_0) at (12+3.5, 9) {};
  	\node[ellipse, fill=gray!40, align=center] (n1_1) at (11.5+3.5, 8) {};
  	\draw[-, line width=1pt] (n1_0) -- (n1_1);
  	\node[rectangle, fill=lmu!40, align=center] (n1_2) at (12.5+3.5, 8) {$k$};
  	\draw[-, line width=1pt] (n1_0) -- (n1_2);
  	\node[ellipse, fill=gray!40, align=center] (n1_3) at (11+3.5, 7) {};
  	\draw[-, line width=1pt] (n1_1) -- (n1_3);
  	\node[rectangle, fill=lmu!40, align=center] (n1_4) at (12+3.5, 7) {$i, j$};
  	\draw[-, line width=1pt] (n1_1) -- (n1_4);
  	\node[rectangle, fill=lmu!40, align=center] (n1_5) at (10.5+3.5, 6) {};
  	\draw[-, line width=1pt] (n1_3) -- (n1_5);
  	\node[ellipse, fill=gray!40, align=center] (n1_6) at (11.5+3.5, 6) {};
  	\draw[-, line width=1pt] (n1_3) -- (n1_6);
  	\node[rectangle, fill=lmu!40, align=center] (n1_7) at (11+3.5, 5) {};
  	\draw[-, line width=1pt] (n1_6) -- (n1_7);
  	\node[rectangle, fill=lmu!40, align=center] (n1_8) at (12+3.5, 5) {};
  	\draw[-, line width=1pt] (n1_6) -- (n1_8);

  	\node[ellipse, fill=gray!40, align=center] (n2_0) at (12+6, 9) {};
  	\node[ellipse, fill=gray!40, align=center] (n2_1) at (11.5+6, 8) {};
  	\draw[-, line width=1pt] (n2_0) -- (n2_1);
  	\node[rectangle, fill=lmu!40, align=center] (n2_2) at (12.5+6, 8) {$k$};
  	\draw[-, line width=1pt] (n2_0) -- (n2_2);
  	\node[rectangle, fill=lmu!40, align=center] (n2_3) at (11+6, 7) {};
  	\draw[-, line width=1pt] (n2_1) -- (n2_3);
  	\node[rectangle, fill=lmu!40, align=center] (n2_4) at (12+6, 7) {$i, j$};
  	\draw[-, line width=1pt] (n2_1) -- (n2_4);

  % \node[] (nw) at (12+8.5, 7)
  % {$w_i = \begin{pmatrix} \vdots \\ w_{ij} = 2 \\ \vdots \\ w_{ik} = 0 \\ \vdots \end{pmatrix}$};
  \end{tikzpicture}
\end{center}

\vspace{1em}
<<pmodel2, eval=FALSE>>=
model_i <- model(endpoint ~ treatment, data,
                 weights = w_i)
@
\end{frame}

%-------------------------------------------------------------------------%

\begin{frame}[fragile]{PRO-ACT Personalised Treatment Effect}



<<pm, fig.height=5, fig.width=11, message=FALSE>>=
set.seed(123)
frst <- pmforest(bmod, data = ALSsurvdata, ntree = 200,
  control = ctree_control(minbucket = 8))
diff_median <- pmodel(x = frst, fun = diffms)
@

<<pm_plot, fig.height=4.5, fig.width=9, out.width="0.75\\textwidth", echo=FALSE>>=
dpdat <- cbind(diff_median, ALSsurvdata)
ggplot(dpdat, aes(x = age, y = diff_median, color = weakness)) +
  geom_point(alpha = 0.2) + ylab(expression(Delta[0.5]))
@
\end{frame}

% -----------------


\begin{frame}{Variable importance}
\begin{center}
\includegraphics[width=0.7\textwidth]{ALSsurv_varimp-1}
\end{center}
\end{frame}



%------------------



\begin{frame}{Is the overall treatment effect good enough?}
\begin{description}
  \item[$H_0:$] Parameters (intercept and treatment effect) are the same for all patients.\\
\end{description}
<<dtest, echo=FALSE, fig.height=4, fig.width=9>>=
library("ggplot2")
set.seed(123)
llbs_diff <- rnorm(100, 2, 20)

yy <- 0.007 
lls <- data.frame(label = c("H[1]"),
                       ll_diff = c(20, 60),
                       y = c(yy + 0.5/400),
                  significant = c("no", "yes"))

lls2 <- rbind(lls, lls)
lls2$yy <- lls2$y - c(Inf, Inf, 0.001, 0.001)

p <- ggplot() +
  geom_line(aes(llbs_diff), stat = "density", trim = TRUE) +
  xlab("difference in log-likelihood between forest \n and base model")

p  +
  geom_line(data = lls2, aes(x = ll_diff, y = yy, group = significant,
                             color = significant), linetype = 2) +
  geom_text(data = lls, aes(label = label, x = ll_diff, y = y,
                            color = significant), parse = TRUE, size = 6,
            show.legend = FALSE) 

@

\end{frame}

\begin{frame}{Is the overall treatment effect good enough?}
\begin{description}
  \item[Survival time $H_0:$] Baseline hazard and treatment effect are the same for all patients.\\
\end{description}
\begin{center}
  \includegraphics[width=0.45\textwidth]{testALSSurv-1}
  \includegraphics[width=0.45\textwidth]{testALSSurv-2}\\
\end{center}
\end{frame}
%-------------------------------------------------------------------------%
%-------------------------------------------------------------------------%


\begin{frame}{Summary}
\begin{itemize}
  \item Data-driven approach for stratified and personalised treatment effect estimation.
  \item Treatment effect estimation using weighted models.
  \item Easy to understand and communicate.\\[2em]
\end{itemize}

Extensions:
\begin{itemize}
  \item Dose-response models
  \item PALM trees
  \item Causal trees and forests
\end{itemize}
\end{frame}


%-------------------------------------------------------------------------%
%-------------------------------------------------------------------------%

% \begin{frame}{It's there to be used!}
% \vspace{2.5em}
% \textbf{\large R packages:}\\[2em]
% \begin{description}
% \item[\texttt{\large partykit}] model-based trees.
% \item[\texttt{\large model4you}] model-based trees and forests for stratified and personalised treatment effects.
% \item[\texttt{\large palmtree}] PALM trees.
% \end{description}
% \end{frame}

\begin{frame}[fragile]{The R Package \texttt{model4you}}
Base model:
<<eval=FALSE>>=
base_model <- model(endpoint ~ treatment, data)
@

Stratified treatment effects:
<<eval=FALSE>>=
strat_models <- pmtree(base_model)
@

Personalised treatment effects:
<<eval=FALSE>>=
pm_forest <- pmforest(base_model)
pers_models <- pmodel(pm_forest)
@
\end{frame}




\thispagestyle{empty}
\begin{frame}


\textbf{Publications:}\\[0.5em]
  \begin{thebibliography}{9} \footnotesize
  \setbeamertemplate{bibliography item}[triangle]

    \bibitem{seibold_model-based_2016}
      H.~{Seibold},  A.~{Zeileis}, T.~{Hothorn}
      \newblock {\em Model-based Recursive Partitioning for Subgroup Analyses}
      \newblock International Journal of Biostatistics, 2016.

    \bibitem{seibold_individual_2017}
      H.~{Seibold},  A.~{Zeileis}, T.~{Hothorn}
      \newblock {\em Individual Treatment Effect Prediction for amyotrophic lateral sclerosis Patients}
      \newblock Statistical Methods in Medical Research, 2017.
%
%     \bibitem{seibold_palmtree_2017}
%       H.~{Seibold}, T.~{Hothorn},  A.~{Zeileis}
%       \newblock {\em Generalised Linear Model Trees with Global Additive Effects}
%       \newblock \url{https://arxiv.org/abs/1612.07498},
%       Under review at Advances in Data Analysis and Classification, 2018.


    \bibitem{seibold_model4you_2017}
      H.~{Seibold},  A.~{Zeileis}, T.~{Hothorn}
      \newblock {\em model4you: An R package for personalised treatment effect estimation}
      \newblock Journal of Open Research Software, 2019.\\[2em]
  \end{thebibliography}
  \textbf{Contact:} \url{heidi.seibold@lmu.de}\\[2em]
  {\footnotesize Slides are licensed under CC-BY 4.0 and available: \url{https://tinyurl.com/yc5ry397}}
\end{frame}


\end{document}
