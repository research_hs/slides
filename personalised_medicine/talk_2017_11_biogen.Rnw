\documentclass[10pt]{beamer}


% gentsroom pics
\usepackage{marvosym}
\definecolor{ccpk}{HTML}{cc00cc}
\definecolor{cctq}{rgb}{0, 204, 255}
\definecolor{ccgr}{HTML}{00cc66}
\definecolor{ccor}{rgb}{230, 138, 0}

% knit from command line with R CMD Sweave
%\VignetteEngine{knitr::knitr}

\input{./input/head}
% \input{./input/commands}

%% above frame title 
\addheadboxtemplate{\color[rgb]{1,1,1}}{\color{uzh} \underline{{\hspace{3pt}\includegraphics[scale=0.33]{uzh_logo_e_pos} 
\hspace{0.7\paperwidth}
\color{black} \tiny Heidi Seibold} \hspace{0.1\paperwidth}}}

%% figures path
\graphicspath{{figure/}}

%% Title
\author{Heidi Seibold}
\title{Personalized Medicine via Interaction Trees and Random Forests}
%\subtitle{Stratified and personalised medicine using model-based recursive
%partitioning}
\date{}
\institute[]{Epidemiology, Biostatistics and Prevention Institute\\[1em] 
\includegraphics[width=0.3\textwidth]{uzh_logo_e_pos}\\[1em]
Joint work with Torsten Hothorn (UZH) and Achim Zeileis (UIBK), sponsored by SNF\\[1em]
Presentation at Biogen Symposium November 2017
}


\begin{document}
<<setup, echo=FALSE>>=
library("knitr")
knitr::opts_chunk$set(cache = TRUE)
library("survival")
library("ggplot2")
theme_set(theme_classic(base_size = 18))
library("gridExtra")

library("xtable")
@



\setbeamercolor{bgr}{fg=black,bg=uzh}

\thispagestyle{empty}
\begin{frame}
\transsplithorizontalout
%\vspace*{1,5cm}
\titlepage
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%


\begin{frame}{Stratified and Personalised Medicine}

\textbf{Overall treatment effect}\\[0.5em]
{ \huge
\textcolor{uzh}{
\Ladiesroom \Gentsroom \Gentsroom \Ladiesroom \Gentsroom \Ladiesroom \Ladiesroom
\Ladiesroom \Gentsroom \Gentsroom \Ladiesroom \Gentsroom \Ladiesroom \Ladiesroom
}
}
\\[2em]

\visible<2->{\textcolor{uzh}{\textbf{Part I: Stratified medicine using model-based trees\\}}}
\textbf{Stratified treatment effect}\\[0.5em]
{ \huge
\textcolor{uzh}{
\Ladiesroom \Gentsroom \Gentsroom \Ladiesroom \Gentsroom 
}
\textcolor{lmu}{
\Gentsroom \Gentsroom \Ladiesroom \Ladiesroom \Gentsroom 
}
\textcolor{red}{
\Ladiesroom \Ladiesroom \Gentsroom \Ladiesroom \Gentsroom 
}
}
\\[2em]


\visible<2->{\textcolor{uzh}{\textbf{Part II: Personalised medicine using model-based forests\\}}}
\textbf{Personalised treatment effect}\\[0.5em]
{\huge
\textcolor{ccpk}{\Ladiesroom}  
\textcolor{uzh}{\Gentsroom}
\textcolor{blue}{\Ladiesroom}  
\textcolor{cctq}{\Gentsroom} 
\textcolor{lmu}{\Ladiesroom}
\textcolor{ccgr}{\Gentsroom} 
\textcolor{ccor}{\Ladiesroom} 
\textcolor{red}{\Ladiesroom} 
}
\end{frame}

\begin{frame}{Overall Treatment Effect}
{\Large
\color{uzh}
\Ladiesroom \Gentsroom \Gentsroom \Ladiesroom \Gentsroom \Ladiesroom \Ladiesroom
\Ladiesroom \Gentsroom \Gentsroom \Ladiesroom \Gentsroom \Ladiesroom \Ladiesroom\\[.5em]
}
Estimate $\boldsymbol{\vartheta} = \begin{pmatrix} \text{intercept} \\ \text{treatment effect} \end{pmatrix}$ via
$$
        \operatornamewithlimits{argmax}\limits_{\vartheta}
         \sum\limits_{j=1}^N 
        \ell((y, \mathbf{x})_j,
        {\boldsymbol\vartheta})
$$
\vspace{2em}\\
Linear predictor for patient $\textcolor{uzh}{\Large\text{\Ladiesroom}_i}$: 
$$
\eta_i = \mathbf{x}_i^\top \boldsymbol{\vartheta}
$$
\end{frame}

%-------------------------------------------------------------------------%
%-------------------------------------------------------------------------%

\begin{frame}\thispagestyle{empty}
\begin{center}
\huge
Part I: Stratified Medicine\\[1.5em]
\visible<2->{Theory}
\end{center}
\end{frame}


\begin{frame}{Stratified Treatment Effects}
\begin{center}
  \resizebox{0.9\textwidth}{!}{
\begin{tikzpicture}
  \node[ellipse, fill=gray!40, align=center] (n0) at (12, 9) {};
	\node[rectangle, fill=uzh!20, align=center] (n1) at (11, 7.4) {\textcolor{uzh}{\Ladiesroom \Gentsroom \Gentsroom \Ladiesroom \Gentsroom}};
	\draw[-, line width=1pt] (n0) -- (n1);
	\node[ellipse, fill=gray!40, align=center] (n2) at (12.5, 8.2) {};
	\draw[-, line width=1pt] (n0) -- (n2);
	\node[rectangle, fill=lmu!20, align=center] (n3) at (12, 7.4) {\textcolor{lmu}{\Gentsroom \Gentsroom \Ladiesroom \Ladiesroom \Gentsroom}};
	\draw[-, line width=1pt] (n2) -- (n3);
	\node[rectangle, fill=red!20, align=center] (n4) at (13, 7.4) {\textcolor{red}{\Ladiesroom \Ladiesroom \Gentsroom \Ladiesroom \Gentsroom}};
	\draw[-, line width=1pt] (n2) -- (n4);
	\node at (17, 8.2) {\small $w_{gj} = \begin{cases} 1 & \text{if \Ladiesroom}_j \text{ in subgroup g}\\ 0 & \text{else}\end{cases} $};
\end{tikzpicture}
}\\[1.8em]
\end{center}
Patient $\textcolor{lmu}{\Large\text{\Ladiesroom}_i}$ with characteristics $\textcolor{lmu}{z_i}$ $\rightarrow$ group \textcolor{lmu}{g}:\\[0.5em]
$$
        \operatornamewithlimits{argmax}\limits_{\vartheta_{\textcolor{lmu}{g}}}
         \sum\limits_{j=1}^N \textcolor{lmu}{w_{gj}} \cdot
        \ell((y, \mathbf{x})_j,
        {\boldsymbol\vartheta}_{\textcolor{lmu}{g}})
$$
\vspace{0.1em}\\
$$
\eta_i 
= \mathbf{x}_i^\top \boldsymbol{\vartheta}\textcolor{lmu}{(z_i)} 
= \mathbf{x}_i^\top \boldsymbol{\vartheta}_{\textcolor{lmu}{g}} 
$$
\end{frame}

\begin{frame}[fragile]{How to find the Partitions?}
Test of independence between the \textbf{partial score functions} and each \textbf{patient characteristic} $z_j$:

<<scores, echo=FALSE, fig.height=3>>=
library("sandwich")
set.seed(225)

## get legend
# https://github.com/hadley/ggplot2/wiki/Share-a-legend-between-two-ggplot2-graphs
g_legend <- function(a.gplot){
  tmp <- ggplot_gtable(ggplot_build(a.gplot))
  leg <- which(sapply(tmp$grobs, function(x) x$name) == "guide-box")
  legend <- tmp$grobs[[leg]]
  return(legend)
}

## data
d <- factor(sample(0:1, 200, replace = TRUE))
z <- rnorm(200)
y <- 1.9 + 0.2 * I(d == 1) + 1.8 * I(z < 0) + 3.6 * I((z > 0) & (d == 1)) + rnorm(200, 0, 0.7)
data <- data.frame(y, d, z)
data$d <- factor(data$d, labels = c("C", "A"))

## model
mod <- lm(y ~ d)
estf <- as.data.frame(estfun(mod))
names(estf) <- c("s_0", "s_1")
pd <- cbind(data, estf)

## plots
p <- ggplot(pd, aes(y = s_0, x = z)) + geom_point(aes(shape = d)) +  theme_bw() +
  geom_hline(yintercept = 0, colour = "grey", size = 1, alpha = 0.5) + ylim(-3.1, 3.1) +
  guides(colour=FALSE) + ylab(expression(psi[alpha])) + xlab(expression(z[j])) +
  scale_shape_discrete(solid = FALSE) + theme(legend.title=element_blank()) 
mylegend <- g_legend(p)
p <- p + theme(legend.position="none")

q <- ggplot(pd, aes(y = s_1, x = z)) + geom_point(aes(shape = d)) + theme_bw() + 
  geom_hline(yintercept = 0, colour = "grey", size = 1, alpha = 0.5) + ylim(-3.1, 3.1) +
  guides(colour=FALSE) + ylab(expression(psi[beta])) + xlab(expression(z[j])) +
  scale_shape_discrete(solid = FALSE) + theme(legend.position="none")

grid.arrange(arrangeGrob(p, q, ncol = 2), 
             mylegend, ncol = 2, widths = c(10, 1))
@
\begin{itemize}
\item Partition if global p-value smaller than significance level
\item Use as split variable the one with the smallest p-value
\end{itemize}
\end{frame}

%-------------------------------------------------------------------------%

\begin{frame}\thispagestyle{empty}
\begin{center}
\huge
Part I: Stratified Medicine\\[1.5em]
Analysis of Effect of Riluzole in ALS Patients
\end{center}
\end{frame}

\begin{frame}{PRO-ACT database\footnote{\url{https://nctu.partners.org/ProACT}}}
\begin{columns}
\column{0.6\textwidth}
\begin{itemize}
\item 23 phase 2 clinical trials
\item Riluzole versus no treatment
\item Primary endpoints of interest:\\
\begin{itemize}
\item	ALS Functional Rating Scale (ALSFRS) 
\item	Survival time
\end{itemize}
\end{itemize}

\column{0.4\textwidth}
\hspace*{-2em}\resizebox{1.2\textwidth}{!}{
\begin{tikzpicture}[mindmap, concept color=uzh!50, font=\sf]

  \tikzstyle{level 1 concept}+=[font=\sf \small, level distance = 8.5em]
  \tikzset{every node/.append style={scale=0.65}}
 
  \node[concept, font=\Large] {ALSFRS\\ 0 - 40}
    child[concept color=gray!20, grow = 72]{ node[concept]{speech}}
    child[concept color=gray!20, grow = 36]{ node[concept]{salivation}}
    child[concept color=gray!20, grow = 0]{ node[concept]{swallowing}}
    child[concept color=gray!20, grow = 324]{ node[concept]{handwriting}}
    child[concept color=gray!20, grow = 288]{ node[concept]{cutting food and handling utensils}}
    child[concept color=gray!20, grow = 252]{ node[concept]{dressing and hygiene}}
    child[concept color=gray!20, grow = 216]{ node[concept]{turning in bed and adjusting bed clothes}}
    child[concept color=gray!20, grow = 180]{ node[concept]{walking}}
    child[concept color=gray!20, grow = 144]{ node[concept]{climbing stairs}}
    child[concept color=gray!20, grow = 108]{ node[concept]{breathing}};
\end{tikzpicture}
}
\end{columns}

\end{frame}

<<ALSsurv_mod, echo=FALSE, warning=FALSE, message=FALSE>>=
load("data/ALSsurvdata.rda")

## model
basemodel_ALSsurv <- survreg(Surv(survival.time, cens) ~ Riluzole,
                             data = ALSsurvdata, dist = "weibull")

## coefficients
estimate <- c(coef(basemodel_ALSsurv), "Log(scale)" = log(basemodel_ALSsurv$scale))
cf_ALSsurv <- cbind(estimate = estimate,
                    "2.5 %" = estimate - qnorm(0.975) * sqrt(diag(basemodel_ALSsurv$var)),  
                    "97.5 %" = estimate + qnorm(0.975) * sqrt(diag(basemodel_ALSsurv$var)))
                    

## plot
pr0 <- predict(basemodel_ALSsurv, newdata = list(Riluzole = "No"),
               type = "quantile", p = seq(.01, .99, by = .01))
pr0 <- data.frame(Riluzole = "No", pr = pr0, prob = seq(.99, .01, by=-.01))

pr1 <- predict(basemodel_ALSsurv, newdata = list(Riluzole = "Yes"),
               type = "quantile", p = seq(.01, .99, by = .01))
pr1 <- data.frame(Riluzole = "Yes", pr = pr1, prob = seq(.99, .01, by=-.01))

pr <- rbind(pr0, pr1)
pr$Riluzole <- factor(pr$Riluzole, c( "No", "Yes"))


p_ALSsurv <- ggplot(data = pr, aes(x = pr, y = prob, group = Riluzole)) +
  geom_line(aes(colour = Riluzole)) +
  labs(x = "time in days", y = "Survivor\nfunction") +
  xlim(0, 1000) +
  ylim(0,1) + 
  theme(legend.position = "none")
@

<<ALSFRS_mod, echo=FALSE, message=FALSE>>=
load("data/ALSFRSdata.rda")

## model
basemodel_ALSFRS <- glm(ALSFRS.halfYearAfter ~ Riluzole + offset(log(ALSFRS.Start)),
                        data = ALSFRSdata, family = gaussian(link = "log"))

## coefficients
cf_ALSFRS <- cbind(estimate = coef(basemodel_ALSFRS), 
                   confint(basemodel_ALSFRS))

## plot
sigma <- sqrt(summary(basemodel_ALSFRS)$dispersion)
means <- predict(basemodel_ALSFRS,
                 newdata = data.frame(Riluzole = c("No", "Yes"),
                                      ALSFRS.Start = median(ALSFRSdata$ALSFRS.Start)),
                 type = "response")
# means <- exp(cumsum(coef(basemodel_ALSFRS)))

q <- 1:40
cdf_trt <- lapply(means, pnorm, q = q, sd = sigma)

cdf <- data.frame(cdf = unlist(cdf_trt), q = rep(q, times = 2), Riluzole = rep(c("No", "Yes"), each = length(q)))

p_ALSFRS <- ggplot(data = cdf, aes(x = q, y = cdf, group = Riluzole)) +
  geom_line(aes(colour = Riluzole)) +
  labs(x = bquote(ALSFRS[6]/median(ALSFRS[0])), y = "Cumulative distr.\nfunction") +
  ylim(0,1) + 
  theme(legend.justification = c(0,1), legend.position = c(0.1, 1))
@


\begin{frame}[fragile]{PRO-ACT Overall Treatment Effects}
\vspace{0.5em} 
\textcolor{uzh}{ALSFRS}:
<<ALSFRS, echo=FALSE, out.width="\\textwidth", fig.width=9, fig.height=2.7, warning=FALSE, results='asis'>>=
rown <- c(expression(paste(" ", alpha, sep = "") ), expression(paste(" ", beta, sep = "") ))
tab <- tableGrob(round(cf_ALSFRS, 2), rows = rown, theme = ttheme_minimal(base_size = 15,
                                                                           core = list(fg_params=list(hjust=1, x=0.95))))

grid.arrange(p_ALSFRS, tab, ncol = 2)
@
\vspace{1em}
\textcolor{uzh}{Survival time}:
<<ALSsurv, echo=FALSE, out.width="\\textwidth", fig.width=9, fig.height=2.7, warning=FALSE, results='asis'>>=
rown <- c(expression(paste(" ", alpha[1], sep = ""), paste(" ", beta, sep = ""), paste(" ", log(alpha[2]), sep = "")))
tab <- tableGrob(round(cf_ALSsurv, 2), rows = rown, 
                 theme = ttheme_minimal(base_size = 15,
                                        core = list(fg_params = list(hjust = 1, x = 0.95))))

grid.arrange(p_ALSsurv, tab, ncol = 2)
@
\end{frame}


\begin{frame}{PRO-ACT Stratified Treatment Effects: ALSFRS}
\hspace*{-3em}
\includegraphics[width = 1.15\textwidth]{figure4.pdf}
\end{frame}

\begin{frame}{PRO-ACT Stratified Treatment Effects: Survival time}
\hspace*{-3em}
\includegraphics[width = 1.15\textwidth]{figure5.pdf}
\end{frame}


%-------------------------------------------------------------------------%
%-------------------------------------------------------------------------%
\begin{frame}\thispagestyle{empty}
\begin{center}
\huge
Part II: Personalised Medicine\\[1.5em]
\visible<2->{Theory}
\end{center}
\end{frame}


\begin{frame}{Personalised Treatment Effects}
\begin{center}
{\huge
\textcolor{ccpk}{\Ladiesroom}  
\textcolor{uzh}{\Gentsroom}
\textcolor{blue}{\Ladiesroom}  
\textcolor{cctq}{\Gentsroom} 
\textcolor{lmu}{\Ladiesroom}
\textcolor{ccgr}{\Gentsroom} 
\textcolor{ccor}{\Ladiesroom} 
\textcolor{red}{\Ladiesroom} 
}\\[1.8em]
\end{center}
Patient $\textcolor{ccpk}{\Large\text{\Ladiesroom}_i}$ with characteristics $\textcolor{ccpk}{z_i}$:\\[0.5em]
$$
        \operatornamewithlimits{argmax}\limits_{\vartheta_{\textcolor{ccpk}{i}}}
         \sum\limits_{j=1}^N \textcolor{ccpk}{w_{ij}} \cdot
        \ell((y, \mathbf{x})_j,
        {\boldsymbol\vartheta}_{\textcolor{ccpk}{i}})
$$
\vspace{0.1em}\\
$$
\eta_i 
= \mathbf{x}_i^\top \boldsymbol{\vartheta}\textcolor{ccpk}{(z_i)} 
= \mathbf{x}_i^\top \boldsymbol{\vartheta}_{\textcolor{ccpk}{i}} 
$$
\end{frame}

\begin{frame}[fragile]{How to get the weights for \textcolor{ccpk}{\huge$\text{\Ladiesroom}_{i}$} ?}
\vspace{.5em}
\begin{center}
  \begin{tikzpicture}
  	\node[ellipse, fill=gray!40, align=center] (n0) at (12, 9) {};
  	\node[rectangle, fill=uzh!40, align=center] (n1) at (11.5, 8) {$i$};
  	\draw[-, line width=1pt] (n0) -- (n1);
  	\node[ellipse, fill=gray!40, align=center] (n2) at (12.5, 8) {};
  	\draw[-, line width=1pt] (n0) -- (n2);
  	\node[rectangle, fill=uzh!40, align=center] (n3) at (12, 7) {$j$};
  	\draw[-, line width=1pt] (n2) -- (n3);
  	\node[rectangle, fill=uzh!40, align=center] (n4) at (13, 7) {$k$};
  	\draw[-, line width=1pt] (n2) -- (n4);
  	
  	\node[ellipse, fill=gray!40, align=center] (n1_0) at (12+3.5, 9) {};
  	\node[ellipse, fill=gray!40, align=center] (n1_1) at (11.5+3.5, 8) {};
  	\draw[-, line width=1pt] (n1_0) -- (n1_1);
  	\node[rectangle, fill=uzh!40, align=center] (n1_2) at (12.5+3.5, 8) {$k$};
  	\draw[-, line width=1pt] (n1_0) -- (n1_2);
  	\node[ellipse, fill=gray!40, align=center] (n1_3) at (11+3.5, 7) {};
  	\draw[-, line width=1pt] (n1_1) -- (n1_3);
  	\node[rectangle, fill=uzh!40, align=center] (n1_4) at (12+3.5, 7) {$i, j$};
  	\draw[-, line width=1pt] (n1_1) -- (n1_4);
  	\node[rectangle, fill=uzh!40, align=center] (n1_5) at (10.5+3.5, 6) {};
  	\draw[-, line width=1pt] (n1_3) -- (n1_5);
  	\node[ellipse, fill=gray!40, align=center] (n1_6) at (11.5+3.5, 6) {};
  	\draw[-, line width=1pt] (n1_3) -- (n1_6);
  	\node[rectangle, fill=uzh!40, align=center] (n1_7) at (11+3.5, 5) {};
  	\draw[-, line width=1pt] (n1_6) -- (n1_7);
  	\node[rectangle, fill=uzh!40, align=center] (n1_8) at (12+3.5, 5) {};
  	\draw[-, line width=1pt] (n1_6) -- (n1_8);
  	
  	\node[ellipse, fill=gray!40, align=center] (n2_0) at (12+6, 9) {};
  	\node[ellipse, fill=gray!40, align=center] (n2_1) at (11.5+6, 8) {};
  	\draw[-, line width=1pt] (n2_0) -- (n2_1);
  	\node[rectangle, fill=uzh!40, align=center] (n2_2) at (12.5+6, 8) {$k$};
  	\draw[-, line width=1pt] (n2_0) -- (n2_2);
  	\node[rectangle, fill=uzh!40, align=center] (n2_3) at (11+6, 7) {};
  	\draw[-, line width=1pt] (n2_1) -- (n2_3);
  	\node[rectangle, fill=uzh!40, align=center] (n2_4) at (12+6, 7) {$i, j$};
  	\draw[-, line width=1pt] (n2_1) -- (n2_4);
  	
  \node[] (nw) at (12+8.5, 7) 
  {$w_i = \begin{pmatrix} \vdots \\ w_{ij} = 2 \\ \vdots \\ w_{ik} = 0 \\ \vdots \end{pmatrix}$};
  \end{tikzpicture}
\end{center}

\vspace{1em}
$$
        \operatornamewithlimits{argmax}\limits_{\vartheta_{\textcolor{ccpk}{i}}}
         \sum\limits_{j=1}^N \textcolor{ccpk}{w_{ij}} \cdot
        \ell((y, \mathbf{x})_j,
        {\boldsymbol\vartheta}_{\textcolor{ccpk}{i}})
$$
\end{frame}

\begin{frame}{Is the overall treatment effect good enough?}
\begin{description}
  \item[$H_0:$] Intercept and treatment effect are the same for all patients.\\
\end{description}
<<test, echo=FALSE, fig.width=12>>=
library("ggplot2")
set.seed(123)
llbs_diff <- rnorm(100, 2, 20)

yy <- 0.007 
lls <- data.frame(label = c("H[1]"),
                       ll_diff = c(20, 60),
                       y = c(yy + 0.5/400),
                  significant = c("no", "yes"))

lls2 <- rbind(lls, lls)
lls2$yy <- lls2$y - c(Inf, Inf, 0.001, 0.001)

p <- ggplot() +
  geom_line(aes(llbs_diff), stat = "density", trim = TRUE) +
  xlab("difference in log-likelihood between forest \n and base model")

p  +
  geom_line(data = lls2, aes(x = ll_diff, y = yy, group = significant,
                             color = significant), linetype = 2) +
  geom_text(data = lls, aes(label = label, x = ll_diff, y = y,
                            color = significant), parse = TRUE, size = 6,
            show.legend = FALSE) 

@

\end{frame}

%-------------------------------------------------------------------------%

\begin{frame}\thispagestyle{empty}
\begin{center}
\huge
Part II: Personalised Medicine\\[1.5em]
Analysis of Effect of Riluzole in
ALS Patients
\end{center}
\end{frame}

\begin{frame}
\includegraphics[width=0.48\textwidth]{results-dp2}
\includegraphics[width=0.48\textwidth]{results-dp_surv1}
\end{frame}

\begin{frame}{Is the overall treatment effect good enough?}
\begin{description}
  \item[ALSFRS $H_0:$] Intercept and treatment effect are the same for all patients.\\
\end{description}
\begin{center} \scriptsize
  \includegraphics[width=0.5\textwidth]{testALSFRS-1}
  \includegraphics[width=0.5\textwidth]{testALSFRS-2}\\
\end{center}
\end{frame}


\begin{frame}{Is the overall treatment effect good enough?}
\begin{description}
  \item[Survival time $H_0:$] Baseline hazard and treatment effect are the same for all patients.\\
\end{description}
\begin{center} \scriptsize
  \includegraphics[width=0.5\textwidth]{testALSSurv-1}
  \includegraphics[width=0.5\textwidth]{testALSSurv-2}\\
\end{center}
\end{frame}

%-------------------------------------------------------------------------%
%-------------------------------------------------------------------------%
\begin{frame}\thispagestyle{empty}
\begin{center}
\huge
Summary
\end{center}
\end{frame}

\begin{frame}{Summary}
\hspace{-2.2em}\begin{tabular}{lll}
\textbf{Overall} & 
$\operatornamewithlimits{argmax}\limits_{\vartheta} \sum\limits_{j=1}^N 
\ell((y, \mathbf{x})_j, {\boldsymbol\vartheta})$ &
$\eta_i = \mathbf{x}_i^\top \boldsymbol{\vartheta}$ \\[2em]
\textbf{Stratified} &
$\operatornamewithlimits{argmax}\limits_{\vartheta_{\textcolor{lmu}{g}}}
\sum\limits_{j=1}^N \textcolor{lmu}{w_{gj}} \cdot \ell((y, \mathbf{x})_j,
{\boldsymbol\vartheta}_{\textcolor{lmu}{g}})$ &
$\eta_i 
= \mathbf{x}_i^\top \boldsymbol{\vartheta}\textcolor{lmu}{(z_i)} 
= \mathbf{x}_i^\top \boldsymbol{\vartheta}_{\textcolor{lmu}{g}}$ \\[2em]
\textbf{Personalised} &
$\operatornamewithlimits{argmax}\limits_{\vartheta_{\textcolor{ccpk}{i}}}
\sum\limits_{j=1}^N \textcolor{ccpk}{w_{ij}} \cdot \ell((y, \mathbf{x})_j,
{\boldsymbol\vartheta}_{\textcolor{ccpk}{i}})$ &
$\eta_i 
= \mathbf{x}_i^\top \boldsymbol{\vartheta}\textcolor{ccpk}{(z_i)} 
= \mathbf{x}_i^\top \boldsymbol{\vartheta}_{\textcolor{ccpk}{i}}$
\end{tabular}
\end{frame}

\begin{frame}{Summary}
\begin{itemize}
  \item Data-driven approach for stratified and personalised medicine.
  \item Easy to understand and communicate.\\[2em]
\end{itemize}

Extensions:
\begin{itemize}
  \item Dose-response models
  \item Mixed models
  \item PALM trees \visible<2->{$\longrightarrow$ do we have time for an excursion? \includegraphics[height=1.5em]{palm_small}}
\end{itemize}
\end{frame}

%-------------------------------------------------------------------------%
%-------------------------------------------------------------------------%

\begin{frame}\thispagestyle{empty}
\begin{center}
\huge
Excursion \includegraphics[height=4em]{palm_small}\\[2em]
\Large
Partially Additive Linear Model Trees\\ =\\ PALM trees
\end{center}
\end{frame}

\begin{frame}{GLM Tree versus PALM Tree}
\begin{tabular}{ll}
GLM Tree &
$\eta_i 
= \mathbf{x}_i^\top \boldsymbol{\vartheta}_{\textcolor{lmu}{g}}
= \mathbf{x}_i^\top \boldsymbol{\vartheta}\textcolor{lmu}{(z_i)} $\\[2em]

PALM Tree &
$\eta_i 
= \mathbf{x}_i^\top \boldsymbol{\vartheta}_{\textcolor{lmu}{g}}
= \mathbf{x}_i^\top \begin{pmatrix}
\boldsymbol{\beta}_{\textcolor{lmu}{g}}\\
\boldsymbol{\gamma}\\
\end{pmatrix}
= \mathbf{x}_{Vi}^\top \boldsymbol{\beta}\textcolor{lmu}{(z_i)} + \mathbf{x}_{Fi}^\top \boldsymbol{\gamma}$
\end{tabular}
\end{frame}

\begin{frame}{PALM Tree for math exam success}
\includegraphics[width=\textwidth]{math1-1}
\end{frame}

\begin{frame}\thispagestyle{empty}
\begin{center}
\huge
End of excursion \includegraphics[height=4em]{palm_small}
\end{center}
\end{frame}

%-------------------------------------------------------------------------%
%-------------------------------------------------------------------------%
{
\usebackgroundtemplate{\includegraphics[height=1\paperheight]{end}}
\begin{frame}\thispagestyle{empty}
\end{frame}
}

\appendix
\section*{Bibliography}
\small
\thispagestyle{empty}
\begin{frame}
\textbf{Publications:}\\[0.2em]
  \begin{thebibliography}{9}
  \setbeamertemplate{bibliography item}[triangle]
      
    \bibitem{seibold_model-based_2016}
      H.~{Seibold},  A.~{Zeileis}, T.~{Hothorn}
      \newblock {\em Model-based Recursive Partitioning for Subgroup Analyses}
      \newblock International Journal of Biostatistics, 2016.
  
    \bibitem{seibold_individual_2017}
      H.~{Seibold},  A.~{Zeileis}, T.~{Hothorn}
      \newblock {\em Individual Treatment Effect Prediction for amyotrophic lateral sclerosis Patients}
      \newblock Statistical Methods in Medical Research, 2017.
      
    \bibitem{seibold_palmtree_2017}
      H.~{Seibold}, T.~{Hothorn},  A.~{Zeileis}
      \newblock {\em Generalised Linear Model Trees with Global Additive Effects}
      \newblock \url{https://arxiv.org/abs/1612.07498}, 2017.
  \end{thebibliography}
\vspace{2.5em}
\textbf{Code:} \hspace{1em} \texttt{partykit}, \texttt{model4you}\\[1em]
\textbf{Contact:} \hspace{1em} \url{heidi.seibold@uzh.ch}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%


\backupbegin
\begin{frame}\thispagestyle{empty}
\begin{center}
\huge
Backup slides
\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%


\begin{frame}{Variable importance}
1. \underline{Tree log-likelihood}
\begin{itemize}
	\item Select the out-of-bag data $\mathcal{L}_t^c$ and determine the
	subgroup to which each observation $i$ belongs.
	\item Compute the log-likelihood contribution of each observation
	$i \in \mathcal{L}_t^c$ based on the respective model in the subgroup.
	\item Compute the out-of-bag log-likelihood as the sum of the
	contributions
	$$
		l_t =
		\sum\limits_{i \in \mathcal{L}_t^c} l((y, \mathbf{x})_i,
		\hat{\boldsymbol\vartheta}(\mathbf{z}_i)).
	$$
\end{itemize}

2. \underline{Variable importance}
$$
	\text{VI}_j = \frac{1}{T} \sum\limits_{t=1}^T
	\left[ l_t - l_t^{(j)}	\right]
$$
$l_t^{(j)}$ log-likelihood with variable $j$ permuted.
\end{frame}


<<ALSFRS_varimp, echo=FALSE, warning=FALSE, fig.show="hide", fig.width=8, fig.height=6>>=
load("data/ALSFRS_varimp.rda")

VI$variable <- factor(VI$variable, levels = rev(levels(VI$variable)))

ggplot(VI, aes(y = VI, x = variable)) + geom_bar(stat = "identity", width = .1) + 
    # coord_flip() + 
  theme(panel.grid.major.x = element_blank(), 
        axis.text.x = element_text(size = 13, angle = 90, hjust = 1, vjust = 0.5),
        axis.title.x = element_blank())
@

\begin{frame}{Variable importance: ALSFRS}
\includegraphics[width=0.95\textwidth]{ALSFRS_varimp-1}

\end{frame}




<<ALSsurv_varimp, echo=FALSE, warning=FALSE, fig.show="hide", fig.width=8, fig.height=6>>=
load("data/ALSsurv_varimp.rda")

VI$variable <- factor(VI$variable, levels = rev(levels(VI$variable)))

ggplot(VI, aes(y = VI, x = variable)) + geom_bar(stat = "identity", width = .1) + 
    # coord_flip() + 
  theme(panel.grid.major.x = element_blank(), 
        axis.text.x = element_text(size = 13, angle = 90, hjust = 1, vjust = 0.5),
        axis.title.x = element_blank())
@

\begin{frame}{Variable importance: Survival time}
\includegraphics[width=0.9\textwidth]{ALSsurv_varimp-1}
\end{frame}
\backupend





\end{document}
