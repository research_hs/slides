\documentclass[10pt, xcolor={table}]{beamer}
\usepackage[]{graphicx}
\usepackage[]{color}
%% maxwidth is the original width if it is less than linewidth
%% otherwise use linewidth (to make sure the graphics do not exceed the margin)
\makeatletter
\def\maxwidth{ %
  \ifdim\Gin@nat@width>\linewidth
    \linewidth
  \else
    \Gin@nat@width
  \fi
}
\makeatother

\usepackage{listings}


\newcommand{\backupbegin}{
   \newcounter{framenumberappendix}
   \setcounter{framenumberappendix}{\value{framenumber}}
}
\newcommand{\backupend}{
   \addtocounter{framenumberappendix}{-\value{framenumber}}
   \addtocounter{framenumber}{\value{framenumberappendix}} 
}



\usepackage{alltt}

\usepackage{url,verbatim}
\usepackage[english]{babel}

\usepackage{avant}
\renewcommand*\familydefault{\sfdefault} %% Only if the base font of the document is to be sans serif
\usepackage[T1]{fontenc}

\usepackage{tikz}
\usetikzlibrary{positioning,shapes,arrows,decorations.pathreplacing,calc,automata}
% load extra stuff
% \usetikzlibrary{shapes.misc,matrix,positioning,fit}

% cross out text 
\usepackage[normalem]{ulem}


\usepackage[headheight=22pt]{beamerthemeboxes}
%\usepackage{bbm}
\usepackage{graphicx}
\beamertemplatenavigationsymbolsempty 
\setbeamercovered{transparent}

% \definecolor{redve}{rgb}{0.604,0.008,0.00}
\definecolor{redve}{rgb}{0,0.61,0.245}
% \definecolor{uzh}{rgb}{0.188,0.522,0.306}
\definecolor{uzh}{HTML}{0028a5}
\setbeamertemplate{itemize item}{$\bullet$} 
\setbeamercolor{title}{fg=uzh}
\setbeamercolor{frametitle}{fg=uzh}
\setbeamertemplate{sections/subsections in toc}[ball unnumbered]
\setbeamercolor{section in toc}{fg=uzh,bg=white}
\setbeamercolor{subsection in toc}{fg=uzh,bg=white}
\setbeamercolor{result}{fg=black, bg=yellow}



\addheadboxtemplate{\color[rgb]{1,1,1}}{\color{uzh} \underline{{\hspace{3pt}\includegraphics[scale=0.33]{figure/uzh_logo_e_pos} 
\hspace{0.55\paperwidth}\color{black} \tiny MOB for subgroup analyses} \hspace{11pt}}}

\addfootboxtemplate{\color[rgb]{1,1,1}}{\color{black} %\tiny \quad  %bla 
\hfill \tiny
\insertframenumber / \inserttotalframenumber \hspace{5pt}}


%% set item distance
\newlength{\wideitemsep}
\setlength{\wideitemsep}{\itemsep}
\addtolength{\wideitemsep}{0.3em}
\let\olditem\item
\renewcommand{\item}{\setlength{\itemsep}{\wideitemsep}\olditem}

\graphicspath{{figure/}}

\input{./input/commands}


\author{\textbf{Heidi Seibold}, Achim Zeileis and Torsten Hothorn}
\title[MOB for Subgroup Identification]{Model Partitioning with an
Application to Subgroup
Identification}
\date{June 16, 2015}
\institute[]{Epidemiology, Biostatistics and Prevention Institute\\[1em] 
University of Zurich\\[2em]
\includegraphics[width=0.3\textwidth]{uzh_logo_e_pos}
}
% \titlegraphic{\includegraphics[width=0.3\textwidth]{uzh_logo_e_pos}\hspace*{4.75cm}}


\usepackage{amsmath}
\begin{document}
\setbeamercolor{bgr}{fg=black,bg=uzh}

\thispagestyle{empty}
\begin{frame}
  \transsplithorizontalout
	%\vspace*{1,5cm}
	\titlepage
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%
\section{Subgroup analyses}


\begin{frame}{Subgroup analyses}
Identifying groups of patients for whom the treatment has a different effect than for others.\\[1.5em]
Effect is:
\begin{itemize}
	\item Stronger
	\item Lower
	\item Contrary
\end{itemize}
\end{frame}


\begin{frame}{Subgroup analyses}
\begin{itemize}
	\item[Goal:] Find predictive factors
	\item[$\hat{=}$] covariate $\times$ treatment interactions
\end{itemize} \vspace{1.5em}
% \end{frame}
% \begin{frame}{Predictive and prognostic factors}
{
\scriptsize
\begin{tikzpicture}
	\node[circle, fill=gray!20, text width=1.6cm, align=center] (prog) at (12,9) {prognostic factor};
	\node[circle, fill=uzh!40, text width=1.2cm, align=center] (outc) at (15,9) {primary endpoint};
	\draw[->, line width=3pt] (prog) -- (outc);
	
	\node[circle, fill=gray!20, text width=1.6cm, align=center] (pred) at (6,9) {predictive factor};
	\node[circle, fill=gray!20, text width=1.2cm, align=center] (treat) at (8.7,10.2) {treatment};
	\node[circle, fill=gray!20, text width=1.2cm, align=center] (outc2) at (8.7,7.8) {primary endpoint};
	\draw[->, line width=3pt] (pred) -- (8.5,9);
	\draw[->, line width=3pt, uzh!50] (treat) -- (outc2);
\end{tikzpicture}
}\\[1.5em]
Trees are great for detecting interactions. Combine trees with models (knowledge about treatment effect).\\ 
$\Rightarrow$ Model-based recursive partitioning.
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%
\section{Data}

\begin{frame}{Data}
PRO-ACT database \cite{massachusetts_general_hospital_pooled_2013}\\[1em]
\begin{itemize}
	\item Amyotrophic lateral sclerosis (ALS) patients
	\item Data of several clinical trials
	\item Treatment of interest: Riluzole
	\item Primary endpoints of interest: Survival time\\[2em]
\end{itemize}
Riluzole modestly prolongs life expectancy \\ 
\begin{itemize}
\item[But:] Are there any groups of patients for whom it is better or worse?\\
\item[$\Rightarrow$] Subgroup analyses
\hspace*{7em} \includegraphics[width = 0.2\textwidth]{riluzole.jpg}
\end{itemize}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%

\section{Results}

\begin{frame}{Subgroups with differential Riluzole effect on the survival}
\hspace*{-3em}
\includegraphics[width = 1.15\textwidth]{figure5.pdf}
\end{frame}


\begin{frame}{MOB Basics}
MOB: Model-based recursive partitioning \cite{zeileis_model-based_2008}\\[2em]
Start with model $\model(\theodat, \bftheta)$ with $$\bftheta = \begin{pmatrix}
	\bfalpha\\
	\bfbeta\\
	\bfgamma\\
	\bfnu\\
\end{pmatrix}
\begin{array}{l} 
	\text{intercept(s)}\\
	\text{treatment effect}\\
	\text{other parameter(s) of interest}\\
	\text{nuisance parameter(s)},\\
\end{array}
$$
which fits data $\theodat$.
\end{frame}


\begin{frame}{Model}
\begin{center}
{\large Parametric model: \textbf{Weibull model}}
\end{center}
\vspace{1em}
\begin{align*}
\mP(Y \le y | X = x) = F\left(\frac{\log(y) - \alpha_1 - \beta x_R}
                                         {\alpha_2}\right)\\[0.5em]
\end{align*}

$y$ (right-censored) survival time,\\ 
$F$ cumulative distribution function of Gompertz distribution\\[2em]

$\bfalpha = \begin{pmatrix}\alpha_1 \\ \alpha_2 \end{pmatrix}$ 
defines the shape of the baseline hazard\\[0.6em]
$\Rightarrow$ use as "intercept"

\end{frame}


\begin{frame}{MOB Basics}
Maybe the treatment effect is not the same for all patients, but depends on their characteristics.\\[1em]
\begin{itemize}
	\item[$\Rightarrow$] Find partitions $\{\segb_b\}$ ($b=1,...,B$) based on patient characteristics $\theoz = (\theoz_1,...,\theoz_J) \in \spacz$
	\item[$\Rightarrow$] Fit separate models $\model(\theodat, \bftheta(b))$ in partitions.\\
\end{itemize}\vspace{1em}
{
\center
\footnotesize
\begin{tikzpicture}
	\node[ellipse, fill=gray!40, align=center] (n0) at (12,9) {$\bftheta$};
	
	\node[rectangle, fill=uzh!40, align=center] (n1) at (10,8) {$\bftheta(1)$};
	\draw[-, line width=1pt] (n0) -- (n1);
	
	\node[rectangle, fill=uzh!40, align=center] (n2) at (14,8) {$\bftheta(2)$};
	\draw[-, line width=1pt] (n0) -- (n2);
\end{tikzpicture}\\[3em]
}
% $$\bftheta(b) = (\bfalpha(b), \bfbeta(b), \bfgamma, \bfnu)^\top$$
\end{frame}


\begin{frame}{Subgroups with differential Riluzole effect on the survival}
\hspace*{-3em}
\includegraphics[width = 1.15\textwidth]{figure5.pdf}
\end{frame}




\begin{frame}{How to find the Partitions?}
\begin{itemize}
	\item[$\Rightarrow$] Test of independence between the \textbf{partial score functions} and each \textbf{patient characteristic}:
	\begin{align*}
	H_0^{\alpha,j}&: \quad \psi_\alpha(\theodat, \htheta) \quad\bot\quad \theoz_j\\
	H_0^{\beta,j}&: \quad \psi_\beta(\theodat, \htheta) \quad\bot\quad \theoz_j, \quad j = 1, \dots, J
\end{align*}\\[.3em]
\end{itemize}
{\small
\begin{align*}\label{scoremat}
        \boldsymbol{\psi} &= \begin{pmatrix}
                \psi_{ \alpha}((y, \mathbf{x})_1,  {\boldsymbol{\vartheta}}) & \psi_{ \beta}((y, \mathbf{x})_1,  {\boldsymbol{\vartheta}}) & \cdots   \cr
                % \psi_{ \alpha}((y, \mathbf{x})_2,  {\boldsymbol{\vartheta}}) & \psi_{ \beta}((y, \mathbf{x})_2,  {\boldsymbol{\vartheta}}) & \cdots   \cr
                \vdots & \vdots &  \cr
                \psi_{ \alpha}((y, \mathbf{x})_N,  {\boldsymbol{\vartheta}}) & \psi_{ \beta}((y, \mathbf{x})_N,  {\boldsymbol{\vartheta}}) & \cdots   \cr
        \end{pmatrix}
\end{align*}
}
\vspace{.7em}
\begin{itemize}
	\item Partition if global p-value smaller than significance level
	\item Use as split variable the one with the smallest p-value
%	\item Use as split point the one that comes along with the minimal sum of segmented objective functions
\end{itemize}
\end{frame}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%



\begin{frame}{Why does this work?}
Example \cite{loh_regression_2014}:\\[1em]
\scriptsize
Data generating process:\\
$\theoy|\theox = \obsx, \theoz = \obsz ~\sim~ \normal(1.9 + 0.2 \cdot x_{A}  + 1.8 \cdot \I(z_1 < 0) + 3.6 \cdot \I(z_1 > 0) \cdot x_{A} , ~0.7)$\\[1em]
Linear model:\\
$\theoy|\theox = \obsx \sim \normal(\alpha + \beta \cdot x_{A},~ \sigma^2)$\\[1em]
\hspace*{-3em}\includegraphics[width = 1.15\textwidth]{figure3a.pdf}
\end{frame}


\begin{frame}{Subgroups with differential Riluzole effect on the survival}
\hspace*{-3em}
\includegraphics[width = 1.15\textwidth]{figure5.pdf}
\end{frame}


%-------------------------------------------------------------------------%

\begin{frame}{Summary}

\begin{itemize}
\item MOB partitions a large class of models suitable
      for the treatment effect on the primary endpoint of interest.
\item Score functions capture instabilities and thus help to
      identify predictive and prognostic variables.
\item Easy interpretation and good to communicate to clinicians.
\item Coming soon: model-based forest for personalised medicine.
\end{itemize}

\end{frame}






\appendix
\section*{Bibliography}
\thispagestyle{empty}
\begin{frame}{Literature}
\bibliographystyle{unsrt}
  \begin{thebibliography}{10}
    \beamertemplatearticlebibitems
  \bibitem{massachusetts_general_hospital_pooled_2013}
    Massachusetts General Hospital, Neurological Clinical Research Institute
    \newblock {\em Pooled Resource Open-Access ALS Clinical Trials Database}
    \newblock {\url{https://nctu.partners.org/ProACT/}, 2013.}
    \beamertemplatearticlebibitems
  \bibitem{zeileis_model-based_2008}
  	A.~Zeileis, T.~Hothorn, K.~Hornik
    \newblock {\em Model-Based Recursive Partitioning}
    \newblock Journal of Computational and Graphical Statistics, 2008.
    \beamertemplatearticlebibitems
  \bibitem{loh_regression_2014}
  	W.~Loh, X.~He, M.~Man
    \newblock {\em A regression tree approach to identifying subgroups with differential treatment effects}
    \newblock Statistics in Medicine, 2015.
  \end{thebibliography}
\vspace{3em}
Contact: heidi.seibold@uzh.ch

\end{frame}


\end{document}