sources <- list.files(pattern = ".tex$|.Rnw$")

## read text
txt <- unlist(lapply(sources, readLines))


### Figures
## extract lines with includegraphics
figr <- grep(pattern = "includegraphics", txt, value = TRUE)

## extract (partial) file names
figfl <- gsub(".*\\{|\\}.*", "", figr)

## list file names in owncloud folder
figpath_orig <- "~/ownCloud/Conferences/slides/subgroups_and_forests/figure/"
figfl_orig <- list.files(path = figpath_orig, 
                         pattern = paste(figfl, collapse = "|"))
figfl_orig

## copy
file.copy(from = paste0(figpath_orig, figfl_orig),
          to = "figure/")


### Data
## extract lines with load()
datr <- grep(pattern = "load\\(", txt, value = TRUE)

## extract (partial) file names
datfl <- gsub('.*\\(\\"data/|.*\\(\\"|\\"\\).*', "", datr)

## list file names in owncloud folder
datpath_orig <- "~/ownCloud/Conferences/slides/subgroups_and_forests/data/"
datfl_orig <- list.files(path = datpath_orig, 
                         pattern = paste(datfl, collapse = "|"))

## copy
file.copy(from = paste0(datpath_orig, datfl_orig),
          to = "data/")

