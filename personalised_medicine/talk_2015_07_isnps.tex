\documentclass[10pt, xcolor={table}]{beamer}
\usepackage[]{graphicx}
\usepackage[]{color}
%% maxwidth is the original width if it is less than linewidth
%% otherwise use linewidth (to make sure the graphics do not exceed the margin)
\makeatletter
\def\maxwidth{ %
  \ifdim\Gin@nat@width>\linewidth
    \linewidth
  \else
    \Gin@nat@width
  \fi
}
\makeatother

\usepackage{listings}


\newcommand{\backupbegin}{
   \newcounter{framenumberappendix}
   \setcounter{framenumberappendix}{\value{framenumber}}
}
\newcommand{\backupend}{
   \addtocounter{framenumberappendix}{-\value{framenumber}}
   \addtocounter{framenumber}{\value{framenumberappendix}} 
}



\usepackage{alltt}

\usepackage{url,verbatim}
\usepackage[english]{babel}

\usepackage{avant}
\renewcommand*\familydefault{\sfdefault} %% Only if the base font of the document is to be sans serif
\usepackage[T1]{fontenc}

\usepackage{tikz}
\usetikzlibrary{positioning,shapes,arrows,decorations.pathreplacing,calc,automata}
% load extra stuff
% \usetikzlibrary{shapes.misc,matrix,positioning,fit}

% cross out text 
\usepackage[normalem]{ulem}


\usepackage[headheight=22pt]{beamerthemeboxes}
%\usepackage{bbm}
\usepackage{graphicx}
\beamertemplatenavigationsymbolsempty 
\setbeamercovered{transparent}

% \definecolor{redve}{rgb}{0.604,0.008,0.00}
\definecolor{redve}{rgb}{0,0.61,0.245}
% \definecolor{uzh}{rgb}{0.188,0.522,0.306}
\definecolor{uzh}{HTML}{0028a5}
\setbeamertemplate{itemize item}{$\bullet$} 
\setbeamercolor{title}{fg=uzh}
\setbeamercolor{frametitle}{fg=uzh}
\setbeamertemplate{sections/subsections in toc}[ball unnumbered]
\setbeamercolor{section in toc}{fg=uzh,bg=white}
\setbeamercolor{subsection in toc}{fg=uzh,bg=white}
\setbeamercolor{result}{fg=black, bg=yellow}



\addheadboxtemplate{\color[rgb]{1,1,1}}{\color{uzh} \underline{{\hspace{3pt}\includegraphics[scale=0.33]{uzh_logo_e_pos} 
\hspace{0.55\paperwidth}\color{black} \tiny MOB for subgroup analyses} \hspace{11pt}}}

\addfootboxtemplate{\color[rgb]{1,1,1}}{\color{black} %\tiny \quad  %bla 
\hfill \tiny
\insertframenumber / \inserttotalframenumber \hspace{5pt}}


%% set item distance
\newlength{\wideitemsep}
\setlength{\wideitemsep}{\itemsep}
\addtolength{\wideitemsep}{0.3em}
\let\olditem\item
\renewcommand{\item}{\setlength{\itemsep}{\wideitemsep}\olditem}

\graphicspath{{figure/}}

\input{./input/commands}


\author{\textbf{Heidi Seibold}, Achim Zeileis and Torsten Hothorn}
\title[MOB for Subgroup Identification]{Model-Based Recursive Partitioning for Subgroup Analyses Using Permutation Tests}
\date{July 14th, 2015}
\institute[]{Epidemiology, Biostatistics and Prevention Institute\\[1em] 
University of Zurich\\[2em]
\includegraphics[width=0.3\textwidth]{uzh_logo_e_pos}
}
% \titlegraphic{\includegraphics[width=0.3\textwidth]{uzh_logo_e_pos}\hspace*{4.75cm}}


\usepackage{amsmath}
\begin{document}
\setbeamercolor{bgr}{fg=black,bg=uzh}

\thispagestyle{empty}
\begin{frame}
  \transsplithorizontalout
	%\vspace*{1,5cm}
	\titlepage
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%
\section{Subgroup analyses}


\begin{frame}{Subgroup analyses}
Identifying groups of patients for whom the treatment has a different effect than for others.\\[1.5em]
Effect is:
\begin{itemize}
	\item Stronger
	\item Lower
	\item Contrary
\end{itemize}
\end{frame}


\begin{frame}{Subgroup analyses}
\begin{itemize}
	\item[Goal:] Find predictive factors
	\item[$\hat{=}$] covariate $\times$ treatment interactions
\end{itemize} \vspace{1.5em}
% \end{frame}
% \begin{frame}{Predictive and prognostic factors}
{
\scriptsize
\begin{tikzpicture}
	\node[circle, fill=gray!20, text width=1.6cm, align=center] (prog) at (12,9) {prognostic factor};
	\node[circle, fill=uzh!40, text width=1.2cm, align=center] (outc) at (15,9) {primary endpoint};
	\draw[->, line width=3pt] (prog) -- (outc);
	
	\node[circle, fill=gray!20, text width=1.6cm, align=center] (pred) at (6,9) {predictive factor};
	\node[circle, fill=gray!20, text width=1.2cm, align=center] (treat) at (8.7,10.2) {treatment};
	\node[circle, fill=gray!20, text width=1.2cm, align=center] (outc2) at (8.7,7.8) {primary endpoint};
	\draw[->, line width=3pt] (pred) -- (8.5,9);
	\draw[->, line width=3pt, uzh!50] (treat) -- (outc2);
\end{tikzpicture}
}\\[1.5em]
Trees are great for detecting interactions. Combine trees with models (knowledge about treatment effect).\\ 
$\Rightarrow$ Model-based recursive partitioning.
\end{frame}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%
\section{Data}

\begin{frame}{Data}
PRO-ACT database \cite{massachusetts_general_hospital_pooled_2013}\\[1em]
\begin{itemize}
	\item Amyotrophic lateral sclerosis (ALS) patients
	\item Data of several clinical trials
	\item Treatment of interest: Riluzole
	\item Primary endpoints of interest: Survival time\\[2em]
\end{itemize}
Riluzole modestly prolongs life expectancy \\ 
\begin{itemize}
\item[But:] Are there any groups of patients for whom it is better or worse?\\
\item[$\Rightarrow$] Subgroup analyses
\hspace*{7em} \includegraphics[width = 0.2\textwidth]{riluzole.jpg}
\end{itemize}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%

\section{Results}

\begin{frame}{Subgroups with differential Riluzole effect on the survival}
\hspace*{-3.5em}
\includegraphics[width = 1.2\textwidth]{figure5.pdf}
\end{frame}


\begin{frame}{MOB Basics}
MOB: Model-based recursive partitioning \cite{zeileis_model-based_2008}\\[2em]
Start with model $\model(\theodat, \bftheta)$ with $$\bftheta = \begin{pmatrix}
	\bfalpha\\
	\bfbeta\\
	\bfgamma\\
	\bfnu\\
\end{pmatrix}
\begin{array}{l} 
	\text{intercept(s)}\\
	\text{treatment effect}\\
	\text{other parameter(s) of interest}\\
	\text{nuisance parameter(s)},\\
\end{array}
$$
which fits data $\theodat \in (\spacy, \spacx)$.
\end{frame}


\begin{frame}{Model}
\begin{center}
{\large Parametric model: \textbf{Weibull model}}
\end{center}
\vspace{1em}
\begin{align*}
\mP(Y \le y | X = x) = F\left(\frac{\log(y) - \alpha_1 - \beta x_R}
                                         {\alpha_2}\right)\\[0.5em]
\end{align*}

$y$ (right-censored) survival time,\\ 
$F$ cumulative distribution function of Gompertz distribution\\[2em]

$\bfalpha = \begin{pmatrix}\alpha_1 \\ \alpha_2 \end{pmatrix}$ 
defines the shape of the baseline hazard\\[0.6em]
$\Rightarrow$ use as "intercept"

\end{frame}


\begin{frame}{MOB Basics}
Maybe the treatment effect is not the same for all patients, but depends on their characteristics.\\[1em]
\begin{itemize}
	\item[$\Rightarrow$] Find partitions $\{\segb_b\}$ ($b=1,...,B$) based on patient characteristics $\theoz = (\theoz_1,...,\theoz_J) \in \spacz$
	\item[$\Rightarrow$] Fit separate models $\model(\theodat, \bftheta(b))$ in partitions.\\
\end{itemize}\vspace{1em}
{
\center
% \footnotesize
\begin{tikzpicture}
	\node[ellipse, fill=gray!40, align=center] (n0) at (12,9) {$\bftheta$};
	
	\node[rectangle, fill=uzh!40, align=center] (n1) at (10,8) {$\bftheta(1)$};
	\draw[-, line width=1pt] (n0) -- (n1);
	
	\node[rectangle, fill=uzh!40, align=center] (n2) at (14,8) {$\bftheta(2)$};
	\draw[-, line width=1pt] (n0) -- (n2);
\end{tikzpicture}\\[3em]
}
% $$\bftheta(b) = (\bfalpha(b), \bfbeta(b), \bfgamma, \bfnu)^\top$$
\end{frame}




% \begin{frame}{Predictive and prognostic factors}
% \includegraphics[width = 0.9\textwidth]{Figure2_slides.pdf}
% \end{frame}


\begin{frame}{Partitioning}
How to find the Partitions?\\[1em]
\begin{itemize}
	\item[$\Rightarrow$] Test of independence between the partial score functions and each patient characteristic:
	\begin{align*}
	H_0^{\alpha,j}&: \quad \psi_\alpha(\theodat, \htheta) \quad\bot\quad \theoz_j\\
	H_0^{\beta,j}&: \quad \psi_\beta(\theodat, \htheta) \quad\bot\quad \theoz_j, \quad j = 1, \dots, J
\end{align*}\\[1.5em]
\end{itemize}
$\psi_\alpha, \psi_\beta$ partial derivatives of the log-likelihood with respect to $\bfalpha / \bfbeta$.\\[1.5em]

\begin{itemize}
	\item Partition if global p-value smaller than significance level
	\item Use as split variable the one with the smallest p-value
%	\item Use as split point the one that comes along with the minimal sum of segmented objective functions
\end{itemize}
\end{frame}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------%



\begin{frame}{Why does this work?}
Example \cite{loh_regression_2014}:\\[1em]
\scriptsize
Data generating process:\\
$\theoy|\theox = \obsx, \theoz = \obsz ~\sim~ \normal(1.9 + 0.2 \cdot x_{A}  + 1.8 \cdot \I(z_1 < 0) + 3.6 \cdot \I(z_1 > 0) \cdot x_{A} , ~0.7)$\\[1em]
Linear model:\\
$\theoy|\theox = \obsx \sim \normal(\alpha + \beta \cdot x_{A},~ \sigma^2)$\\[1em]
\hspace*{-3em}\includegraphics[width = 1.15\textwidth]{figure3a.pdf}
\end{frame}


\begin{frame}{Permutation testing}
\vspace{1em}
{\small
Example: Independence test between $\psi_\alpha$ and $z_1$, and $\psi_\beta$ and $z_1$\\[1em]
\begin{itemize}
  \item Compute test statistic:\\
    $\text{cor}(\psi_\alpha, z_1) = -0.01$\\
    $\text{cor}(\psi_\beta, z_1) = 0.45$
  \item Permute $z_1$ many times and compute the test statistic each time.
    % \begin{center}
    \includegraphics[width = 0.9\textwidth]{ptest.pdf}
    % \end{center}
  \item $p$-value $= \frac{\text{number of test statistics more extreme than original}}{\text{number of permutations}}$\\[1.5em]
\end{itemize}
}
\end{frame}


\begin{frame}{Subgroups with differential Riluzole effect on the survival}
\hspace*{-3.5em}
\includegraphics[width = 1.2\textwidth]{figure5.pdf}
\end{frame}


%-------------------------------------------------------------------------%

\begin{frame}{Summary}

\begin{itemize}
\item MOB partitions a large class of models suitable
      for the treatment effect on the primary endpoint of interest.
\item Score functions capture instabilities and thus help to
      identify predictive and prognostic variables.
\item Easy interpretation and good to communicate to clinicians.
\item Coming soon: model-based forest for personalised medicine.
\end{itemize}

\end{frame}






\appendix
\section*{Bibliography}
\thispagestyle{empty}
\begin{frame}{Literature}
\bibliographystyle{unsrt}
  \begin{thebibliography}{10}
    \beamertemplatearticlebibitems
  \bibitem{massachusetts_general_hospital_pooled_2013}
    Massachusetts General Hospital, Neurological Clinical Research Institute
    \newblock {\em Pooled Resource Open-Access ALS Clinical Trials Database}
    \newblock {\url{https://nctu.partners.org/ProACT/}, 2013.}
    \beamertemplatearticlebibitems
  \bibitem{zeileis_model-based_2008}
  	A.~Zeileis, T.~Hothorn, K.~Hornik
    \newblock {\em Model-Based Recursive Partitioning}
    \newblock Journal of Computational and Graphical Statistics, 2008.
    \beamertemplatearticlebibitems
  \bibitem{loh_regression_2014}
  	W.~Loh, X.~He, M.~Man
    \newblock {\em A regression tree approach to identifying subgroups with differential treatment effects}
    \newblock Statistics in Medicine, 2015.
  \end{thebibliography}
\vspace{3em}
Contact: heidi.seibold@uzh.ch

\end{frame}


% \backupbegin
% 
% \begin{frame}[fragile]{Simple implementation}{Weibull model}
% {\scriptsize
% \begin{lstlisting}[basicstyle=\ttfamily]
% 
% library("partykit")
% 
% ## Function to compute Weibull model and return score matrix
% mywb <- function(data, weights, parm) {  
%   mod <- survreg(Surv(survival.time, cens) ~ Riluzole, 
%                  data = data, subset = weights > 0, 
%                  dist = "weibull")
%   ef <- as.matrix(estfun(mod)[,parm])
%   ret <- matrix(0, nrow = nrow(data), ncol = ncol(ef))
%   ret[weights > 0,] <- ef
%   ret
% }
% 
% ## Compute tree
% tree <-  ctree(fm, data = data, ytrafo = mywb, 
%                control = ctree_control(maxdepth = 2, 
%                      testtype = "Bonferroni"))
% \end{lstlisting}
% }
% \end{frame}
% 
% 
% \begin{frame}{3 examples}
% 
% \begin{description}[labelwidth=3em]
%   \item[ALSFRS] ALS functional rating scale\\
% 	sum score that measures patients abilities\\
% 	values 0 - 40
% 	\item[ALSFRS items] 10 items that form the scores\\
% 	each 0 - 4
% 	\item[survival time] right-censored time to event information
% \end{description}
% \end{frame}
% 
% 
% 
% \begin{frame}{ALSFRS example}
% \begin{align*}
% 	\mE\left.\left(\frac{\alsfrs_6}{\alsfrs_0} \right\vert  X = x\right) = \frac{\mE(\alsfrs_6\vert  X = x)}{\alsfrs_0} = \exp\{ \alpha + \beta x_{B} \}
% \end{align*}
% or equivalently
% \begin{align*}
% 	\mE(\alsfrs_6\vert  X = x) = \exp\{ \alpha + \beta x_{B} \} \cdot \exp\{ \log(\alsfrs_0) \}
% \end{align*}\\[2em]
% GLM with log-link and offset
% 
% \end{frame}
% 
% 
% 
% \begin{frame}{ALSFRS example}{Results}
% \includegraphics[width = \textwidth]{figure4.pdf}
% \end{frame}
% 
% 
% %-------------------------------------------------------------------------%
% 
% \begin{frame}{ALSFRS items example}
% 2 components to consider:\\[1em]
% \begin{enumerate}
% 	\item Baseline adjustment\\
% 		Adjust for item score at beginning of treatment\\
% 		$\Rightarrow$ compute seperate models
% 	\item Mulitvariate primary endpoint\\
% 		Look at 10 items simultaneously\\
% 		$\Rightarrow$ compute 10 item-models in every node\\[1em]
% \end{enumerate}
% Score matrix of dimension $n \times p \cdot 10$\\
% \end{frame}
% 
% \begin{frame}{ALSFRS items example}
% each item ordinal with values $0,...,4$\\
% $\Rightarrow$ proportional odds model
% \begin{align*}
% 	P(Y \leq r\vert  X = x) &= \frac{\exp(\alpha_r + \bfx^\top \bfbeta)}{1 + \exp(\alpha_r + \bfx^\top \bfbeta)} \qquad \text{ for }  r = 0,...,c-1
% \end{align*}\\[1.5em]
% adjust for baseline:
% \begin{align*}
% 	P(Y_6 \leq r|Y_0 = k, X = x) &= \frac{\exp(\alpha_{rk} + \bfx^\top \bfbeta_k)}{1 + \exp(\alpha_{rk} + \bfx^\top \bfbeta_k)}
% 	\qquad \text{ for } k=0,...,4
% \end{align*}\\[1em]
% compute also blockwise tests.
% \end{frame}
% 
% 
% \begin{frame}{Results}
% \begin{center}
% \only<1>{
% \includegraphics[width = 0.6\textwidth]{figuretable1.pdf}
% }
% 
% 
% 
% \only<2>{
% \tiny
%     \begin{tabular}{lccrrrr}
% 			&&& \multicolumn{4}{p{0.3\textwidth}}{\includegraphics[width=0.73\textwidth]{figuretable1.pdf}}\\[-2em]
% 			\hline \hline
% 	\multicolumn{1}{c}{Item}&\multicolumn{1}{c}{No.}&\multicolumn{1}{c}{Start}&\multicolumn{1}{p{0.15\textwidth}}{Node 3}&\multicolumn{1}{p{0.15\textwidth}}{Node 4}&\multicolumn{1}{p{0.15\textwidth}}{Node 6}&\multicolumn{1}{p{0.15\textwidth}}{Node 7}\tabularnewline
% \hline
% \cellcolor{white}   Speech&\cellcolor{white}   1&\cellcolor{white}   $0$&\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   \tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $1$&\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   \tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $2$&\cellcolor{white}   &\cellcolor{gray!20}   -0.27&\cellcolor{white}   &\cellcolor{white}   \tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $3$&\cellcolor{gray!20}   0.33&\cellcolor{gray!20}   0.04&\cellcolor{gray!20}   -0.27&\cellcolor{gray!20}   -0.06\tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $4$&\cellcolor{blue!40}   0.84&\cellcolor{gray!20}   0.11&\cellcolor{white}   &\cellcolor{white}   \tabularnewline
% \hline
% \cellcolor{white}   Salivation&\cellcolor{white}   2&\cellcolor{white}   $0$&\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   \tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $1$&\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   \tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $2$&\cellcolor{gray!20}   0.22&\cellcolor{gray!20}   0.43&\cellcolor{gray!20}   1.36&\cellcolor{gray!20}   -0.20\tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $3$&\cellcolor{gray!20}   0.15&\cellcolor{gray!20}   -0.26&\cellcolor{gray!20}   -0.24&\cellcolor{gray!20}   -0.05\tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $4$&\cellcolor{gray!20}   0.49&\cellcolor{gray!20}   -0.03&\cellcolor{white}   &\cellcolor{white}   \tabularnewline
% \hline
% \cellcolor{white}   Swallowing&\cellcolor{white}   3&\cellcolor{white}   $0$&\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   \tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $1$&\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   \tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $2$&\cellcolor{gray!20}   0.35&\cellcolor{gray!20}   -0.89&\cellcolor{gray!20}   1.51&\cellcolor{gray!20}   -0.75\tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $3$&\cellcolor{gray!20}   0.57&\cellcolor{gray!20}   -0.36&\cellcolor{gray!20}   -0.40&\cellcolor{gray!20}   0.35\tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $4$&\cellcolor{blue!40}   0.62&\cellcolor{white}   &\cellcolor{gray!20}   0.15&\cellcolor{gray!20}   0.28\tabularnewline
% \hline
% \cellcolor{white}   Handwriting&\cellcolor{white}   4&\cellcolor{white}   $0$&\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   &\cellcolor{gray!20}   -1.45\tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $1$&\cellcolor{white}   &\cellcolor{gray!20}   -1.15&\cellcolor{gray!20}   -0.36&\cellcolor{gray!20}   -0.08\tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $2$&\cellcolor{white}   &\cellcolor{gray!20}   -0.54&\cellcolor{white}   &\cellcolor{gray!20}   0.04\tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $3$&\cellcolor{gray!20}   -0.13&\cellcolor{gray!20}   0.14&\cellcolor{gray!20}   -0.08&\cellcolor{gray!20}   -0.28\tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $4$&\cellcolor{gray!20}   -0.10&\cellcolor{gray!20}   0.04&\cellcolor{white}   &\cellcolor{gray!20}   -0.14\tabularnewline
% \hline
% \end{tabular}
% 
% }
% 
% 
% \only<3>{
% \tiny
%     \begin{tabular}{lccrrrr}
%     	&&& \multicolumn{4}{p{0.3\textwidth}}{\includegraphics[width=0.73\textwidth]{figuretable1.pdf}}\\[-2em]
% 			\hline \hline
% 	\multicolumn{1}{c}{Item}&\multicolumn{1}{c}{No.}&\multicolumn{1}{c}{Start}&\multicolumn{1}{p{0.15\textwidth}}{Node 3}&\multicolumn{1}{p{0.15\textwidth}}{Node 4}&\multicolumn{1}{p{0.15\textwidth}}{Node 6}&\multicolumn{1}{p{0.15\textwidth}}{Node 7}\tabularnewline
% \hline
% \cellcolor{white}   Cutting&\cellcolor{white}   5&\cellcolor{white}   $0$&\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   \tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $1$&\cellcolor{white}   &\cellcolor{white}   &\cellcolor{gray!20}   -0.01&\cellcolor{gray!20}   -0.79\tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $2$&\cellcolor{white}   &\cellcolor{gray!20}   0.15&\cellcolor{white}   &\cellcolor{gray!20}   0.48\tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $3$&\cellcolor{gray!20}   -0.03&\cellcolor{gray!20}   -0.07&\cellcolor{gray!20}   0.10&\cellcolor{blue!40}   0.52\tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $4$&\cellcolor{gray!20}   0.13&\cellcolor{gray!20}   -0.09&\cellcolor{gray!20}   -0.14&\cellcolor{gray!20}   -0.21\tabularnewline
% \hline
% \cellcolor{white}   Hygiene&\cellcolor{white}   6&\cellcolor{white}   $0$&\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   \tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $1$&\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   \tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $2$&\cellcolor{white}   &\cellcolor{gray!20}   -0.11&\cellcolor{white}   &\cellcolor{gray!20}   -0.37\tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $3$&\cellcolor{gray!20}   -0.22&\cellcolor{magenta!40}   -0.37&\cellcolor{gray!20}   0.14&\cellcolor{gray!20}   0.27\tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $4$&\cellcolor{gray!20}   0.26&\cellcolor{gray!20}   0.01&\cellcolor{gray!20}   0.14&\cellcolor{gray!20}   0.30\tabularnewline
% \hline
% \cellcolor{white}   Bed&\cellcolor{white}   7&\cellcolor{white}   $0$&\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   \tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $1$&\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   \tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $2$&\cellcolor{white}   &\cellcolor{white}   &\cellcolor{gray!20}   -0.03&\cellcolor{gray!20}   0.29\tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $3$&\cellcolor{gray!20}   0.15&\cellcolor{gray!20}   -0.32&\cellcolor{gray!20}   -0.12&\cellcolor{gray!20}   -0.05\tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $4$&\cellcolor{gray!20}   -0.21&\cellcolor{gray!20}   -0.10&\cellcolor{gray!20}   -0.11&\cellcolor{gray!20}   -0.35\tabularnewline
% \hline
% \cellcolor{white}   Walking&\cellcolor{white}   8&\cellcolor{white}   $0$&\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   \tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $1$&\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   \tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $2$&\cellcolor{white}   &\cellcolor{white}   &\cellcolor{gray!20}   0.48&\cellcolor{gray!20}   -0.04\tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $3$&\cellcolor{white}   &\cellcolor{gray!20}   0.11&\cellcolor{white}   &\cellcolor{gray!20}   0.46\tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $4$&\cellcolor{gray!20}   0.51&\cellcolor{gray!20}   0.13&\cellcolor{white}   &\cellcolor{white}   \tabularnewline
% \hline
% \end{tabular}
% 
% }
% 
% 
% \only<4>{
% \tiny
%     \begin{tabular}{lccrrrr}
%     	&&& \multicolumn{4}{p{0.3\textwidth}}{\includegraphics[width=0.73\textwidth]{figuretable1.pdf}}\\[-2em]
% 			\hline \hline
% 	\multicolumn{1}{c}{Item}&\multicolumn{1}{c}{No.}&\multicolumn{1}{c}{Start}&\multicolumn{1}{p{0.15\textwidth}}{Node 3}&\multicolumn{1}{p{0.15\textwidth}}{Node 4}&\multicolumn{1}{p{0.15\textwidth}}{Node 6}&\multicolumn{1}{p{0.15\textwidth}}{Node 7}\tabularnewline
% \hline
% \cellcolor{white}   Stairs&\cellcolor{white}   9&\cellcolor{white}   $0$&\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   \tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $1$&\cellcolor{white}   &\cellcolor{gray!20}   -0.02&\cellcolor{gray!20}   -0.01&\cellcolor{gray!20}   -0.39\tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $2$&\cellcolor{white}   &\cellcolor{white}   &\cellcolor{gray!20}   -0.80&\cellcolor{gray!20}   0.07\tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $3$&\cellcolor{white}   &\cellcolor{gray!20}   0.26&\cellcolor{gray!20}   -0.65&\cellcolor{gray!20}   -0.16\tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $4$&\cellcolor{blue!40}   1.01&\cellcolor{gray!20}   0.06&\cellcolor{gray!20}   0.72&\cellcolor{gray!20}   0.29\tabularnewline
% \hline
% \cellcolor{white}   Respiratory&\cellcolor{white}   10&\cellcolor{white}   $0$&\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   \tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $1$&\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   \tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $2$&\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   \tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $3$&\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   \tabularnewline
% \cellcolor{white}   &\cellcolor{white}   &\cellcolor{white}   $4$&\cellcolor{gray!20}   0.58&\cellcolor{gray!20}   -0.08&\cellcolor{white}   &\cellcolor{white}   \tabularnewline
% \hline
% \end{tabular}
% 	
% }
% \end{center}
% \end{frame}
% 
% 
% 
% %-------------------------------------------------------------------------%
% 
% 
% \begin{frame}{Cox example}
% \begin{align*}
%   \lambda(y|\bfx) = \lambda_0(y) \exp(\beta x_R)
% \end{align*}\\[2em]
% Objective function: negative partial likelihood\\ 
% \hspace{9.5em} (without baseline hazard)\\
% \begin{itemize}
% 	\item[$\Rightarrow$] No classical score function\\
% 	\item[$\Rightarrow$] Use surrogate score function\\
% 		Martingale residuals (as score with respect to the baseline hazard/"intercept")\\
% 		Score residuals (as score with respect to $\beta$)
% \end{itemize}
% 
% \end{frame}
% 
% 
% 
% \begin{frame}{Results}
% 
% \includegraphics[width = \textwidth]{figure6.pdf}
% \end{frame}
% 
% 
% \backupend





\end{document}